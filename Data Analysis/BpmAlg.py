from __future__ import division
import pdb
import sys
import matplotlib
def calc_bpm(beatsTs, skipAlg = False) : #beatTs is a list of timestamps

    #second secs[i] starts at i seconds and contains all beatsTs that happened between i and i + 1 seconds into the recording
    secs = []
    offset = beatsTs[0]
    prev = None
    head = None
    for beatTs in beatsTs:
        secInd = (beatTs - offset) // 1000
        while len(secs) <= secInd : secs.append(Second(len(secs)))

        beat = Beat(secs[secInd], beatTs)
        if prev is not None : prev.add_next(beat)
        else : head = beat
        prev = beat

        secs[secInd].add_beat(beat, beatTs - offset)

    beat = head.next


    #run through the data and use well spaced beats to calculate bpms
    cur = head
    prevWS = None
    while cur.next is not None and cur.next.next is not None :

        #if we have found a string of three well spaced beats
        if check_close(cur.distNext, cur.next.distNext) :
            #use them to calculate a bpm and set it as the bpm for these three beats
            #print 'start: ' + repr(cur)
            streak = 1
            bpm = cur.bpm_from_next(2)
            for i in range(2) :

                if prevWS is not None :
                    prevWS.add_ws_next(cur)
                prevWS = cur

                cur.bpm = bpm
                cur.bpmStreak = streak
                streak += 1
                cur = cur.next

            cur.bpm = bpm
            cur.bpmStreak = streak

            prevWS.add_ws_next(cur)
            prevWS = cur

            streak += 1
            prevL = [cur.prev.prev, cur.prev]
            #while the next beat is within 10% of both the min and max of (up to) the last five beats
            while cur.next is not None and check_close(min_dist(prevL), cur.distNext) and check_close(max_dist(prevL), cur.distNext) :

                prevL.append(cur)
                if len(prevL) > 6 : prevL.pop(0)
                cur = cur.next
                diffT = cur.ts - prevL[0].ts
                numBeats = len(prevL)
                cur.bpm = 60 / ((diffT / numBeats) / 1000)
                if abs(cur.bpm - cur.prev.bpm) > 10 :
                    pdb.set_trace()
                cur.bpmStreak = streak

                prevWS.add_ws_next(cur)
                prevWS = cur

                streak += 1
                if cur.next is None :
                    break
        cur = cur.next
        if cur is None : #python won't let me break nested loops
            break



    #find the head of the well spaced list - it will be the first beat that has a wsNext link
    wsHead = None
    cur = head
    while cur.wsNext is None :
        cur = cur.next
    wsHead = cur


    #Now do a second run through and look for areas were a streak of more than 3 is immdiately followed by a streak
    #of three with a very different bpm. It is very likely that these three are wrong.
    #We assume that a sequence of five well timed beats is always right
    cur = wsHead.wsNext
    while cur.wsNext is not None :
        #if we are going from an at least 5 streak to a new streak
        if cur.wsPrev.bpmStreak >= 5 and cur.bpmStreak == 1 :
            diffT = cur.wsDistPrev
            diffBpm = cur.bpm - cur.wsPrev.bpm
            if abs(diffBpm / (diffT / 1000)) >= 1 :
                #if there are three beats in this seqence, either because there are not four left or because the fourth is in a difference streak
                if not cur.ws_has_next_n(4) or cur.wsNext.wsNext.wsNext.bpmStreak == 1:
                    cur.wsPrev.ws_del_next_n(3)
                    cur = cur.wsPrev #works because of how the deletion is implemented
        if cur.wsNext is None :
            break
        cur = cur.wsNext

    #reapeat the process, but in reverse
    #cur is already the end of the list
    cur = cur.wsPrev
    while cur.wsPrev is not None :
        if cur.wsNext.bpmStreak >= 5 and cur.bpmStreak == 1:
            diffT = cur.wsDistNext
            diffBpm = cur.bpm - cur.wsNext.bpm
            if abs(diffBpm / (diffT / 1000)) >= 1 :
                if not cur.ws_has_prev_n(4) or cur.wsPrev.wsPrev.wsPrev.bpmStreak == 1:
                    cur.wsNext.ws_del_prev_n(3)
                    cur = cr.wsNext
        cur = cur.wsPrev


    streakHead = None
    prevStreak = None
    cur = wsHead
    beatL = []
    while cur.wsNext is not None :
        beatL.append(cur)
        if cur.wsNext is None or cur.bpmStreak > cur.wsNext.bpmStreak :

            newStreak = Streak(beatL)
            if streakHead is None :
                streakHead = newStreak
            else :
                prevStreak.add_next(newStreak)
            prevStreak = newStreak

            beatL = []

        cur = cur.wsNext

    #for testing, ouput lists of timestamps of beats that were deemed well spaced
    beatsL = []
    divL = []
    cur = wsHead
    while cur.wsNext is not None :
        beatsL.append(cur.ts)
        if cur.bpmStreak > cur.wsNext.bpmStreak :
            divL.append(cur.ts + cur.wsDistNext // 2)
        cur = cur.wsNext

    if skipAlg :
        return beatsL, divL

    #return beatsL, divL, beatsL
    curStr = streakHead.next
    prevInc = streakHead #for now, just include the first one, even though it will probably usually be garbage
    beats = [b.ts for b in streakHead.beats]
    bpmL = [(b.ts, b.bpm) for b in streakHead.beats]
    changeObj = Change([streakHead])
    printOn = False
    while curStr is not None :
        root = Node(None, True, prevInc)
        inc = eval_path(curStr, True, changeObj, prevInc, 0, root)
        noInc = eval_path(curStr, False, changeObj.duplic(), prevInc, 0, root)
        root.value = min(inc, noInc)
        if inc < noInc : #if including costs less
            if printOn :
                print '***INCLUDING STREAK***'
            changeObj.include(curStr)
            beats += [b.ts for b in curStr.beats]
            bpmL += [(b.ts, b.bpm) for b in curStr.beats]
            prevInc = curStr
        else :
            if printOn :
                print '***EXCLUDING STREAK***'
        if printOn :
            print 'start: ' + str(curStr.start)
            print 'id: ' + str(curStr.id) + ', size: ' + str(curStr.size)
            print 'bpms: ' + str([b.bpm for b in curStr.beats])
            print 'ave: ' + str(sum([b.bpm for b in curStr.beats]) / len(curStr.beats)) + ', min: ' + str(curStr.minBpm) + ', max: ' + str(curStr.maxBpm)
            print 'time: ' + str((curStr.start - prevInc.stop) / 1000)
        curStr = curStr.next

        if printOn :
            layers = []
            depth = 0
            layers.append([root])
            while depth < 4 and layers[-1][0].left is not None :
                newLayer = []
                depth += 1
                for n in layers[-1] :
                    newLayer.append(n.left)
                    newLayer.append(n.right)
                layers.append(newLayer)
            for l in layers :
                print 'LAYER:'
                for n in l :
                    print n.__repr__(), n.report
        #
        # G = networkx.DiGraph()
        # for l in layers :
        #     for n in l :
        #         G.add_node(n)
        #         if n.parent is not None :
        #             G.add_edge(n.parent, n)


        # graph = networkx.Graph()
        # queue = [root]
        # pdb.set_trace()
        # while len(queue) > 0 :
        #     cur = queue.pop(0)
        #     graph.add_node(cur.value)
        #     #print cur.streak.id
        #     if cur.left is not None :
        #         queue.append(cur.left)
        #         #print 'appending ' + str(cur.left.streak.id)
        #         queue.append(cur.right)
        #         #print 'appending ' + str(cur.right.streak.id)
        #     if cur.parent is not None :
        #         graph.add_edge(cur.parent, cur)

        #pos = networkx.nx_pydot.graphviz_layout(G, prog = 'dot')
        # networkx.draw(G, pos, with_labels = False, arrows = False)
        # matplotlib.pyplot.show()

    #creat list of the form [(time, bpm), ...]

    #bpmL is of the form [(ts, bpm),...]
    return beatsL, divL, beats, bpmL

  

#Given two numbers, check if smaller/bigger is within 0.1 of 1, i.e. that the numbers are roughly within 10% of each other
def check_close(num1, num2) :
    if num1 is None or num2 is None :
        pdb.set_trace()
    return abs(min(num1, num2) / max(num1, num2) - 1) <= 0.075

def eval_path(curStreak, include, changeObj, prevInc, depth, parent) :
    #print 'called on streak id ' + str(curStreak.id)
    #pdb.set_trace()
    if depth >= 8 or curStreak is None: return 0
    if include :
        changeObj.include(curStreak)
        diffT = (curStreak.start - prevInc.stop) / 1000
        diffBpm = abs(curStreak.startBpm - prevInc.stopBpm)
        changeRate = diffBpm / diffT


        rateCost = 5 * changeRate * changeRate + 90 * diffBpm
        changeCost = 0.0005 * changeObj.get_change()

        node = Node(parent, True, curStreak)

        node.report = 'RATE: ' + str(diffBpm) + ',' + str(diffT) + '=>' + str(rateCost)
        node.report += ', CHANGE: ' + str(changeObj.get_change()) + '=>' + str(changeCost)

        cost = rateCost + changeCost
        if curStreak.startBpm < 20 or curStreak.startBpm > 200 :
            node.report = 'BPM OUT OF RANGE'
            cost += sys.maxint #apparently overflow is impossible in python

        inc = eval_path(curStreak.next, True , changeObj, curStreak, depth + 1, node)
        noInc = eval_path(curStreak.next, False , changeObj.duplic(), curStreak, depth + 1, node)
        node.value = cost + min(inc, noInc)
        node.report += ', CHILDREN: ' + str(min(inc, noInc))
        return cost + min(inc, noInc)
    else : #don't include
        node = Node(parent, False, curStreak)
        sizeCost = 0.25 * pow(curStreak.size, 4)
        quietCost = min(10 * (curStreak.start - prevInc.stop) / 1000, 400)
        cost = sizeCost + quietCost
        inc = eval_path(curStreak.next, True , changeObj, prevInc, depth + 1, node)
        noInc = eval_path(curStreak.next, False , changeObj.duplic(), prevInc, depth + 1, node)
        node.value = cost + min(inc, noInc)
        node.report = 'SKIP: ' + str(curStreak.size) + '=>' + str(sizeCost) + ', QUIET: ' + str(curStreak.start - prevInc.stop) + '=>' + str(quietCost) + ', CHILDREN: ' + str(min(inc, noInc))

        return cost + min(inc, noInc)



def add_second(sec, optList) :
    roots = optList[sec.start]

#returns minimum distance between two beats given a list of beats
def min_dist(bList) :
    return min([b.distNext for b in bList][:-1])
def max_dist(bList) :
    return max([b.distNext for b in bList][:-1])

class Node :

    #included means right, not included means left
    def __init__(self, parent, isInclude, streak) :
        self.id = streak.id
        self.inc = isInclude
        self.parent = parent
        self.left = None
        self.right = None
        self.streak = streak
        self.value = -1
        self.report = 'none'
        if parent is None :
            return
        if isInclude :
            parent.right = self
        else :
            parent.left = self
        #print 'created: ' + self.__repr__()

    def __repr__(self) :

        string = 'Node instance for ' + str(self.id) + (' included' if self.inc else ' excluded') + ', value: ' + str(self.value)
        if self.parent is not None :
            string += ', parent: ' + str(self.parent.id) + (' in' if self.parent.inc else ' ex')
        return string
#class for recording the total amount of change over the last 30 seconds
class Change :

    #incStreaks is a list of streaks that have been included
    def __init__(self , incStreaks) :
        self.incStreaks = incStreaks

    def include(self, streak) :
        self.incStreaks.append(streak)
        self.clean()

    #removes any streaks that happened too long ago
    def clean(self) :
        start = self.incStreaks[-1].start - 30000
        self.incStreaks = [x for x in self.incStreaks if x.start >= start]

    def get_change(self) :
        change = 0
        for i in range(len(self.incStreaks) - 1) :
            cur = self.incStreaks[i]
            nex = self.incStreaks[i + 1]
            change += max(abs(cur.maxBpm - nex.minBpm), abs(cur.minBpm - nex.maxBpm))
        return change


  
    def duplic(self) :
        return Change(self.incStreaks[:])
    def duplic_and(self, streak) :
        newL = self.incStreaks
        newL.append(streak)
        return Change(newL)

#Class for encapsulating a streak of well spaced beats
class Streak :
    count = 0
    def __init__(self, wsBeatsL) :
        self.start = wsBeatsL[0].ts
        self.stop = wsBeatsL[-1].ts
        self.size = len(wsBeatsL)
        self.startBpm = wsBeatsL[0].bpm
        self.stopBpm = wsBeatsL[-1].bpm
        self.maxBpm = max(w.bpm for w in wsBeatsL)
        self.minBpm = min(w.bpm for w in wsBeatsL)
        self.beats = wsBeatsL
        self.next = None
        self.id = Streak.count
        Streak.count += 1

    def add_next(self, other) :
        self.next = other
        other.prev = self
#class for holding an optimal solution, given some constraints
class OptNode :

    def __init__(self, secObj, didInclude, left, right) :
        self.secObj = secObj
        self.include = didInclude
        self.left = left
        self.right = right

class Second :

    def __init__(self, start) :
        self.beats = []
        self.start = start #this will be the same as this second's index in the secs list

    def add_beat(self, beat, time) :
        beat.time = time - self.start * 1000
        self.beats.append(beat)

    def update_bpm(self) :
        self.beatBpm = sum(b.bpm for b in self.beats) // len(self.beats)


    def __contains__(self, beatObj) :
        return beatObj in self.beats

    def __le__(self, other) :
        return self.start <= other.start
class Beat :

    def __init__(self, second, ts) :
        self.prev = None
        self.next = None
        self.distPrev = None
        self.distNext = None

        #Used to create a parallel linked of well spaced beats
        self.wsPrev = None
        self.wsNext = None
        self.wsDistNext = None
        self.wsDistPrev = None

        self.ts = ts
        self.sec = second
        self.bpm = None
        self.bpmStreak = None

    def add_next(self, beat) :
        self.next = beat
        beat.prev = self
        self.distNext = beat.ts - self.ts
        beat.distPrev = self.distNext

    def add_ws_next(self, beat) :
        self.wsNext = beat
        beat.wsPrev = self
        self.wsDistNext = beat.ts - self.ts
        beat.wsDistPrev = self.wsDistNext

    def bpm_from_prev(self, num) :
        time = 0
        numBeats = 0
        curBeat = self
        while numBeats < num :
            time += curBeat.prev
            numBeats += 1
            curBeat = curBeat.prev
        return 60 / ((time / numBeats) / 1000)

    def bpm_from_next(self, num) :
        time = 0
        numBeats = 0
        curBeat = self
        while numBeats < num :
            time += curBeat.distNext
            numBeats += 1
            curBeat = curBeat.next
        return 60 / ((time / numBeats) / 1000)

    #returns a list containing self and the next num beats
    def get_next_n(self, num) :
        bList = [self]
        curBeat = self
        for i in range(num) :
            curBeat = curBeat.next
            bList.append(curBeat)
        return bList

    #returns true if there are num well spaced beats after this one
    def ws_has_next_n(self, num) :
        cur = self.wsNext
        for i in range(num) :
            if cur is None :
                return False
            cur = cur.wsNext
        return True
    def ws_has_prev_n(self, num) :
        cur = self.wsPrev
        for i in range(num) :
            if cur is None :
                return False
            cur = cur.wsPrev
        return True

    def ws_del_next_n(self, num) :
        cur = self.wsNext
        for i in range(num) :
            cur = cur.wsNext
        if cur is None :
            self.wsNext = None
            self.wsDistNextdistNext = None
        else :
            self.add_ws_next(cur)

    def ws_del_prev_n(self, num) :
        cur = self.wsPrev
        for i in range(num) :
            cur = cur.wsPrev
        if cur is None :
            self.wsPrev = None
            self.wsDistPrev = None
        else :
            cur.add_ws_next(self)

    # returns a list containing the previous num beats
    def get_prev_n(self, num) :
        bList = []
        curBeat = self
        for i in range(num) :
            curBeat = curBeat.prev
            bList.append(curBeat)
        return bList
class Iterator :

    def __init__(self, secList, beatHead) :
        self.secList = secList
        self.beatHead = beatHead
        self.curInd = 0
        self.curBeat = beatHead

    def next_beat(self) :
        self.curBeat = self.curBeat.next
        return self.curBeat.prev

    def has_next_beat(self) :
        return self.curBeat.next is not None
    def next(self) :
        if curBeat is None or self.secList[curInd] <= self.curBeat.sec :
            curInd += 1
            return sec[curInd - 1]
        else :
            curBeat = curBeat.next
            return curBeat.prev
    def has_next(self) :
        return self.curInd < len(self.secList) or self.curBeat is not None

# if __name__ == '__main__' :
#     #run some tests
#     sec0 = Second(0)
#     sec1 = Second(1)
#     sec2 = Second(2)
#
#     b400 = Beat(sec0, 400)
#     b900 = Beat(sec0, 900)
#     b1200 = Beat(sec1, 1200)
#     b1500 = Beat(sec1, 1500)
#
#     b400.add_next(b900)
#     b900.add_next(b1200)
#     b1200.add_next(b1500)
#
#     if '[400, 900, 1200]' != [x.ts for x in b400.get_next_n(2)] :
