from __future__ import division
from study2excel import Person, Activity, OrderGroup
import re
import os
import pdb
import cPickle as pickle
import BpmAlg
import openpyxl as xl
from openpyxl.styles import PatternFill, Alignment
import numpy as np
import imp
import traceback
import matplotlib
from matplotlib.figure import Figure
#from multiprocessing import Process, Pipe
import thread
try :
    from kinLog import Frame, Joint
    import KinDisplay
    import KinGraph
except ImportError :
    print 'VISUAL IMPORT ERROR: kinect display not available'
from TimeInstance import TimeIns

import HRDisplay

class ComPipe :
    def __init__(self) :
        self.tMillis = None
        self.selJoints = []
class PeopleManager :

    def __init__(self) :
        self.people = None

    def __getitem__(self, key) :
        return self.pDict[key]

    def save_to_file(self) :
        for person in self.people :
            if hasattr(person, 'kinFrames') :
                print 'Error: attempting to pickle a person with kinFrames attached'
                return
        with open('people_file.pkl', 'wb') as outF :
            pickle.dump(self.people, outF, pickle.HIGHEST_PROTOCOL)

    def load_from_file(self) :
        with open('people_file.pkl', 'rb') as inF :
            self.people = pickle.load(inF)
        self.check_fully_formed()
        self.pDict = {p.name : p for p in self.people}

    def add_ages(self) :
        names = ''
        ages = ''
        names = names.split()
        ages = ages.split()
        for name, age in zip(names, ages) :
            self.pDict[name].age = age
            print 'setting ' + self.pDict[name].name + ' to ' + str(age)

    def into_order_groups(self) :
        names = ['SFH','SHF', 'HFS', 'HSF', 'FHS', 'FSH']
        self.ordGroups = []
        for name, people in zip(names, zip(*[iter(self.people[3:])]*3)) :
            self.ordGroups.append(OrderGroup(name, list(people)))


    def rename_rest4(self) :
        for person in self.people :
            for act in person.activities :
                if act.name == 'Rest 4' :
                    act.name = 'Portal Rest'

    def kin_display(self, name) :
        person = self.pDict[name]
        person.kinFrames_from_disk()
        print 'kinFrames loaded'
        KinDisplay.display(person)
        print 'returning from kin display'

    def kin_both(self, name) :
        person = self[name]
        person.kinFrames_from_disk()
        comPipe = ComPipe()
        #thread.start_new_thread(KinDisplay.display, (person, comList))
        thread.start_new_thread(KinGraph.Display, (person, comPipe))
        #pro = Process(target = KinDisplay.display, args = (person, child_conn))
        #pro.start()
        KinDisplay.display(person, comPipe)


    def kin_graph(self, name) :
        person = self[name]
        person.kinFrames_from_disk()
        KinGraph.Display(person)

    def hr_display(self, name) :
        HRDisplay.Display(self[name])

    #assume order is squat, fruit, holo, portal
    def borg_from_lines(self, lines) :
        for ind in range(len(lines[0])) :
            person = self[lines[0][ind]]
            person.actDict['Squat'].dataDict['Borg'] = int(lines[1][ind])
            person.actDict['Fruit Ninja'].dataDict['Borg'] = int(lines[2][ind])
            person.actDict['Holopoint'].dataDict['Borg'] = int(lines[3][ind])
            person.actDict['Portal'].dataDict['Borg'] = int(lines[4][ind])


    #from timetable
    def import_person_with_times(self, lines) :
        p = Person()
        p.activities = []
        p.name, p.gameOrder = lines[0].split()[0:2]
        for act, times in zip(*[iter(lines[1:])]*2) : #belive it or not this is idomatic
            act = act.strip()
            tups = re.findall(r'(\d+:\d+:\d+)', times)
            tups = [t if len(t) == 8 else '0' + t for t in tups] #assume times of form h:mm:ss or hh:mm:ss
            actObj = Activity(act, TimeIns.hms(tups[0]), TimeIns.hms(tups[1]))
            p.activities.append(actObj)
        p.allocate_rests()
        self.people.append(p) #assumes people list already exists
        print 'created:\n', str(p)


    def load_people_from_timetable(self, fname) :
        study = open(fname, 'r')
        people = []
        line = study.readline()
        while len(line) != 0 :
            p = Person()
            p.activities = []
            p.name, p.gameOrder = line.split()[0:2]
            line = study.readline()
            while not line.isspace() and len(line) != 0:
                name = line.strip()
                line = study.readline()
                tups = re.findall(r'(\d+:\d+(?::\d+)?)', line)
                if len(tups) < 2 : continue
                for ind, time in enumerate(tups) :
                    if tups[ind][1] == ':' : tups[ind] = '0' + tups[ind]
                    if len(tups[ind]) == 5 : tups[ind] = tups[ind] + ':00'
                start = TimeIns.hms(tups[0])
                end = TimeIns.hms(tups[1])
                act = Activity(name, start, end)
                p.activities.append(act)
                line = study.readline()

            p.allocate_rests()
            people.append(p)
            print 'CREATED:\n', str(p)

            while line.isspace() :
                line = study.readline()
        try :
            self.people += people
        except TypeError :
            self.people = []
            self.people += people

    def add_5m_squat(self, name) :
        person = self[name]
        squAct = [x for x in person.activities if x.name == 'Squat'][0]
        fiveAct = Activity('', squAct.begin, TimeIns(squAct.begin.millis + 5 * 60000))
        fiveAct.name = 'Squat Five' #needed to avoid the renaming logic in the contructor.
        fiveAct.bpmL = [x for x in squAct.bpmL if x[0] - squAct.bpmL[0][0] <= 5 * 60000]
        person.activities.insert(person.activities.index(squAct) + 1, fiveAct)

    # def add_5m_squat(self) :
    #     for person in self.people :
    #         squAct = [x for x in person.activities if x.name == 'Squat'][0]
    #         fiveAct = Activity('', squAct.begin, TimeIns(squAct.begin.millis + 5 * 60000))
    #         fiveAct.name = 'Squat Five' #needed to avoid the renaming logic in the contructor.
    #         fiveAct.bpmL = [x for x in squAct.bpmL if x[0] - squAct.bpmL[0][0] <= 5 * 60000]
    #         person.activities.insert(person.activities.index(squAct) + 1, fiveAct)

    def time_ard0_from_dir(self, direc) :
        for fname in os.listdir(direc) :
            person = [p for p in self.people if p.name.lower() in fname.lower()][0]
            self.time_of_ard0(person.name, fname)

    def time_of_ard0(self, name, fname) :
        with open(fname, 'r') as f :
            for _ in range(10) :
                next(f)
            line = next(f)
            hms = re.search(r'\d{2}:\d{2}:\d{2}.\d{3}', line).group(0)
            hms= TimeIns.hms(hms)
            ts = int(re.search(r'Beat: (\d+)', line).group(1))
            self[name].timeArd0 = TimeIns(hms.millis - ts) #time of day when the arduino timestamps was 0 ms


    def person_hr_from_file(self, name, fname) :
        person = self[name]
        fileStr = open(fname, 'r').read()
        tuples = re.findall(r'(\d{2}:\d{2}\:\d{2}).\d+ Band Beat: (\d+)', fileStr)
        hmsStart = TimeIns.hms(tuples[0][0])
        tsStart = int(tuples[1][1])
        beats = [int(t[1]) for t in tuples]
        person.beats = beats
        person.bpmList = BpmAlg.calc_bpm(beats)[3]

        newBpm = self.interpolate_denser_bpm(person.bpmList)
        for act in person.activities :
            start = act.begin - hmsStart
            stop = act.end - hmsStart
            act.bpmL = [(x[0], TimeIns(x[0] - tsStart + hmsStart.millis).get_hms(), x[1]) for x in newBpm if x[0] - tsStart >= start and x[0] - tsStart <= stop]

    def load_hr_data_from_direc(self, direc) :
        os.chdir('./' + direc)
        fnames = os.listdir('./')
        for person in self.people :
            print 'processsing', person.name

            fname = [f for f in fnames if person.name.lower() in f.lower()]
            if len(fname) != 1 :
                print 'length error on person: ', person.name, len(fname)
                continue
            fname = fname[0]
            hrFile = open(fname, 'r')
            fileStr = hrFile.read()

            tuples = re.findall(r'(\d{2}:\d{2}\:\d{2}).\d+ Band Beat: (\d+)', fileStr)
            hmsStart = TimeIns.hms(tuples[0][0])
            tsStart = int(tuples[1][1])
            beats = [int(t[1]) for t in tuples]
            person.beats = beats
            person.bpmList = BpmAlg.calc_bpm(beats)[3]

            newBpm = PeopleManager.interpolate_denser_bpm(person.bpmList)
            for act in person.activities :
                start = act.begin - hmsStart #minus is overloaded
                ind = 0
                relBpm = []

                #perfect opportunity to use binary search
                while newBpm[ind][0] - tsStart < start :
                    ind += 1

                stop = act.end - hmsStart
                while ind < len(newBpm) and newBpm[ind][0] - tsStart < stop :
                    relBpm.append((newBpm[ind][0], TimeIns(newBpm[ind][0] - tsStart + hmsStart.millis).get_hms(), newBpm[ind][1]))
                    ind += 1

                act.bpmL = relBpm
        os.chdir('..')

    def load_kinect_from_direc(self, direc) :
        os.chdir(direc)
        fnames = os.listdir('./')
        tZero = None
        for person in self.people :
            print 'processing', person.name

            fname = [f for f in fnames if person.name.lower() in f.lower()]
            if len(fname) != 1 :
                print 'length error on person', person.name, len(fname)
                continue
            fname = fname[0]

            frames = []
            with open(fname, 'r') as fi :
                for line in fi :
                    skelId = re.search(': (\d+) \(\(', line)
                    if not skelId : continue
                    skelId = int(skelId.group(1))

                    time = re.search(r'\d{2}:\d{2}:\d{2}.\d{3}', line)

                    if time :
                        time = TimeIns.hms(time.group(0))
                    else :
                        continue

                    tZero = tZero if tZero is not None else time

                    jointTups = re.findall(r'JID:\d(\d{2}), LOC3:\{(.*?)\}', line)
                    if len(jointTups) == 0 : continue
                    joints = []
                    for tup in jointTups :
                        xyz = re.findall(r'[XYZ]=([^, ]+)', tup[1])
                        joints.append(Joint(int(tup[0]), float(xyz[0]), float(xyz[1]), float(xyz[2])))
                    frames.append(Frame(time, joints, skelId))
                    #print str(len(frames))
            person.kinFrames = frames
            person.kinFrames_to_disk()

    def remove_neg_ones(self) :
        for p in self.people :
            for act in p.activities :
                if hasattr(act, 'dataDict') :
                    for key in act.dataDict.keys() :
                        if act.dataDict[key] == -1 :
                            del act.dataDict[key]


    def check_fully_formed(self) :
        for person in self.people :
            try :
                print person.name
                if hasattr(person, 'age') : print person.age
                if hasattr(person, 'gameOrder') : print person.gameOrder
                for act in person.activities :
                    print '\t' + act.name + ', ' + str(len(act.bpmL))
                print 'Frames length:', len(person.kinFrames)
            except (AttributeError, TypeError) as e:
                print e

    #MIGHT BE BROKEN
    #Takes list of the form [(ts, bpm)...] and interpolates to give at least one bpm measurement per frequency
    #frequency is milliseconds
    @staticmethod
    def interpolate_denser_bpm(bpmL, frequency = 1000) :
        newBpm = []
        for ind, (ts, bpm) in enumerate(bpmL) :
            if ind == 0 :
                newBpm.append((ts, bpm))
            prevTime = bpmL[ind - 1][0]
            while ts - prevTime > frequency :
                t = prevTime + frequency
                distPrev = abs(t - bpmL[ind - 1][0])
                distNext = abs(t - ts)
                wAve = distNext * bpmL[ind - 1][1] + distPrev * bpm
                wAve /= (distPrev + distNext)
                newBpm.append((t, wAve))
                prevTime += frequency
            newBpm.append((ts, bpm))
        return newBpm

    def reorder_people(self) :
        peopleOrder = ''.split()
        print peopleOrder
        ordList = []
        for pName in peopleOrder :
            person = [p for p in self.people if p.name == pName]
            if len(person) != 1 :
                print 'cant find ' + pName
                continue
            ordList.append(person[0])
        self.people = ordList

    def graphs_as_images(self) :
        os.makedirs('Graphs')
        os.chdir('./Graphs')
        for person in self.people :
            HRDisplay.Display(person, True)

    def write_spreadsheet(self) :
        wb = xl.Workbook()

        #write a sheet for each person
        for person in self.people :
            ws = wb.create_sheet(person.name)
            ws.cell(row = 1, column = 1).value = person.name + ', ' + person.gameOrder
            ind = 2
            for act in person.activities :
                ws.cell(row = ind, column = 1).value = act.name + ', ' + act.begin.get_hms() + ' - ' + act.end.get_hms()
                ind += 1
                last = 0
                act.sheetBpmL = []
                for ts, hms, bpm in act.bpmL :
                    if ts - last > 2000 : #only add a new number every 2 seconds
                        ws.cell(row = ind, column = 1).value = hms
                        ws.cell(row = ind, column = 2).value = int(bpm)
                        act.sheetBpmL.append((ts, int(bpm)))
                        ind += 1
                        last = ts

            ws['G18'] = 'Summary HR'
            ws['H19'] = 'MAX'
            ws['I19'] = 'MIN'
            ws['J19'] = 'AVG'
            ws['K19'] = 'AVG final min'
            ws['L19'] = 'TIME (mins)'

            curRow = 20
            useWave = False
            for act in person.activities :
                ws.cell(row = curRow, column = 7).value = act.name
                act.maxHR = int(max(act.bpmL, key = lambda t : t[2])[2])
                ws.cell(row = curRow, column = 8).value = act.maxHR
                act.minHR = int(min(act.bpmL, key = lambda t : t[2])[2])
                ws.cell(row = curRow, column = 9).value = act.minHR

                if useWave :
                    Wave = 0
                    prevTs = 0
                    for ts, hms, bpm in act.bpmL :
                        if prevTs == 0 :
                            prevTs = ts
                            continue
                        Wave += (ts - prevTs) * bpm
                        prevTs = ts
                    Wave /= (act.bpmL[-1][0] - act.bpmL[0][0])
                    Wave = int(Wave)
                    act.aveHR = Wave
                    ws.cell(row = curRow, column = 10).value = Wave
                else :
                    act.aveHR = sum(x[1] for x in act.sheetBpmL) // len(act.sheetBpmL)
                    ws.cell(row = curRow, column = 10).value = act.aveHR

                lastTs = act.sheetBpmL[-1][0]
                lastMin = [x for x in act.sheetBpmL if x[0] >= lastTs - 60000]
                lastMinAve = sum(x[1] for x in lastMin) // len(lastMin)
                act.lastMinAve = lastMinAve
                ws.cell(row = curRow, column = 11).value = lastMinAve
                ws.cell(row = curRow, column = 12).value = act.end.diff_mins(act.begin)

                curRow += 1

        actOrder = ['Tutorial', 'Squat', 'Squat Five', 'Squat Rest', 'Fruit Ninja', 'Fruit Ninja Rest', 'Holopoint', 'Holopoint Rest', 'Portal']
        rests = ['Squat Rest', 'Fruit Ninja Rest', 'Holopoint Rest', 'Portal Rest']
        games = ['Squat', 'Fruit Ninja', 'Holopoint', 'Portal']
        self.sheet_for_method(wb, 'Average HR', actOrder, Activity.average_hr, True)
        self.sheet_for_method(wb, 'Max HR', actOrder, Activity.max_hr, True)
        self.sheet_for_method(wb, 'Max 2 sec', actOrder, lambda act : Activity.max_over_millis(act, 2000), True)
        self.sheet_for_method(wb, 'Max 10 sec', actOrder, lambda act : Activity.max_over_millis(act, 10000), True)
        self.sheet_for_method(wb, 'Max 30 sec', actOrder, lambda act : Activity.max_over_millis(act, 30000), True)
        self.sheet_for_method(wb, 'Max 1 min', actOrder, lambda act : Activity.max_over_millis(act, 60000), True)
        self.sheet_for_method(wb, 'Play Time', actOrder, Activity.play_time)
        self.sheet_for_method(wb, 'Rest HR 2 min', rests, lambda act : Activity.rest_hr(act, 2), True)
        self.sheet_for_method(wb, 'Rest HR 4 min', rests, lambda act : Activity.rest_hr(act, 4), True)

        #moderate is 50% max or above
        #vigorous is 70% max or above
        #max is 220 - age
        self.sheet_for_method(wb, 'Moderate (50%) Seconds', games, lambda act : Activity.blocks_at_frac(act, 0.5))
        self.sheet_for_method(wb, 'Vigorous (70%) Seconds', games, lambda act : Activity.blocks_at_frac(act, 0.7))
        self.all_sheet(wb, actOrder)

        wb.save('hrSheet.xlsx')

    def all_sheet(self, wb, actOrder) :
        ws = wb.create_sheet('ALL')

        row = 1
        col = 2
        for person in self.people[3:] :
            ws.cell(row = row, column = col).value = person.name
            col += 1
        col = 1
        row = 2

        for actName in actOrder :
            col = 1
            ws.cell(row = row, column = col).value = actName
            col += 1
            for person in self.people[3:] :
                try :
                    act = person.actDict[actName]
                    val = act.dataDict['Max 10 sec']
                    ws.cell(row = row, column = col).value = val
                except :
                    pass
                col += 1
            row += 1

        row += 1
        for actName in actOrder :
            col = 1
            ws.cell(row = row, column = col).value = actName
            col += 1
            for person in self.people[3:] :
                try :
                    act = person.actDict[actName]
                    val = act.dataDict['Rest HR 4 min']
                    ws.cell(row = row, column = col).value = val
                except :
                    pass
                col += 1
            row += 1

        row += 1
        for actName in ['Squat', 'Fruit Ninja', 'Holopoint', 'Portal'] :
            col = 1
            ws.cell(row = row, column = col).value = actName
            col += 1
            for person in self.people[3:] :
                try :
                    act = person.actDict[actName]
                    val = act.dataDict['Borg']
                    ws.cell(row = row, column = col).value = val
                except :
                    pass
                col += 1
            row += 1

    def clear_data_dict(self) :
        for person in self.people :
            for act in person.activities :
                try :
                    del act.dataDict
                except :
                    pass

    def sheet_for_method(self, wb, name, actOrder, method, colorHR = False) :
        print 'processing', repr(method)
        ws = wb.create_sheet(name)

        row = 1
        col = 2
        for person in self.people :
            ws.cell(row = row, column = col).value = person.name
            col += 1
        col = 1
        row = 2
        for actName in actOrder :
            col = 1
            ws.cell(row = row, column = col).value = actName
            col += 1
            for person in self.people :
                try :
                    act = [act for act in person.activities if act.name.lower() == actName.lower()][0]
                except IndexError :
                    col += 1
                    print 'index error for ', actName
                    continue

                try :
                    val = act.dataDict[name]
                except :
                    if not hasattr(act, 'dataDict') :
                        act.dataDict = {}
                    act.person = person
                    val = method(act)
                    del act.person
                    if val is None :
                        col += 1
                        continue
                    act.dataDict[name] = val

                ws.cell(row = row, column = col).value = val
                if colorHR and hasattr(person, 'age') :
                    frac = val / person.max_hr()
                    color = ''
                    if frac >= 0.9 :
                        color = 'fc4646'
                    elif frac >= 0.8 :
                        color = 'fc8646'
                    elif frac >= 0.7 :
                        color = 'f6fc46'

                    if len(color) != 0 :
                        ws.cell(row = row, column = col).fill = PatternFill(start_color = color, end_color = color, fill_type = 'solid')
                #del act.person
                col += 1
            row += 1

        col = 5
        self.into_order_groups()
        for ordGroup in self.ordGroups :
            ws.merge_cells(start_row = row, start_column = col, end_row = row, end_column = col + 2)
            ws.cell(row = row, column = col).value = ordGroup.name
            ws.cell(row = row, column = col).alignment = Alignment(horizontal = 'center')
            col += 3
        row += 1
        for actName in actOrder :
            ws.cell(row = row, column = 1).value = actName
            col = 5
            for group in self.ordGroups :
                vals = []
                for person in group.people :
                    try :
                        act = [act for act in person.activities if act.name.lower() == actName.lower()][0]
                    except IndexError as e :
                        print e
                        continue
                    vals.append(act.dataDict[name])
                try :
                    if isinstance(vals[0], basestring) :
                        vals = [60 * int(v[1]) + int(v[2]) for v in [v.split(':') for v in vals]]
                        ave = sum(vals) // len(vals)
                        ave = '0:{:02d}:{:02d}'.format(*divmod(ave, 60))
                    else :
                        ave = sum(vals) // len(vals)
                except BaseException as e:
                    print e
                    ave = -1
                ws.merge_cells(start_row = row, start_column = col, end_row = row, end_column = col + 2)
                ws.cell(row = row, column = col).value = ave
                ws.cell(row = row, column = col).alignment = Alignment(horizontal = 'center')
                col += 3
            row += 1

        col = len(self.people) + 2
        row = 1
        ws.cell(row = row, column = col).value = 'AVG'
        ws.cell(row = row, column = col + 1).value = 'SD'
        row += 1
        for actName in actOrder :

            values = [p.actDict[actName].dataDict[name] for p in self.people if actName in p.actDict and name in p.actDict[actName].dataDict] #comeon lucky short circuiting
            #pdb.set_trace()
            try :
                if isinstance(values[0], basestring) :
                    values = [60 * int(v[1]) + int(v[2]) for v in [v.split(':') for v in values]]
                    avg = sum(values) // len(values)
                    std = int(np.array(values).std())
                    avg = '0:{:02d}:{:02d}'.format(*divmod(avg, 60))
                    std = '0:{:02d}:{:02d}'.format(*divmod(std, 60))
                else :
                    avg = sum(values) // len(values)
                    std = int(np.array(values).std())
            except BaseException as e:
                print e
                avg = -1
                std = -1
            ws.cell(row = row, column = col).value = avg
            ws.cell(row = row, column=col + 1).value = std
            row += 1
