import re
import os
import time
import pdb
from TimeInstance import TimeIns

def open_logs() :
    logs = []

    for root, dirs, files in os.walk('./') :
        logs += [os.path.join(root, f) for f in files if f.endswith('.log') or f.endswith('.txt')]

    print logs



    # for fname in os.listdir('./') :
    #     if os.path.isdir(fname) :
    #         path = os.getcwd() + '\\' + fname
    #         os.chdir(path)
    #
    # logs = [f for f in os.listdir(os.getcwd()) if '.txt' in f or '.log' in f]
    for i in range(len(logs)) :
        print 'logs[' + str(i) + '] = ' + logs[i]
    return logs



def open_file(fname) :
    t_zero = None
    frames = []
    with open(fname) as f :
        for line in f:
            time_tups = re.findall(r'(\d{2}):(\d{2}):(\d{2}).(\d{3})', line)
            if len(time_tups) == 0 : continue

            t_millis = (int(time_tups[0][0]) * 60 * 60 + int(time_tups[0][1]) * 60 + int(time_tups[0][2])) * 1000 + int(time_tups[0][3])
            if t_zero is None : t_zero = t_millis

            joint_tups = re.findall(r'JID:\d(\d{2}), LOC3:\{(.*?)\}', line)
            joints = []
            for tup in joint_tups :
                xyz = re.findall(r'[XYZ]=([^, ]+)', tup[1])
                #print tup, xyz
                joints.append(joint(int(tup[0]), float(xyz[0]), float(xyz[1]), float(xyz[2])))
            frames.append(frame(t_millis - t_zero, joints, TimeIns(t_millis)))
    return frames

class Frame :
    def __init__(self, time, joints, skelId) :
        self.timeIns = time
        self.joints = joints
        self.skelId = skelId

    def __repr__(self) :
        string = '<Frame at ' + str(hex(id(self))) + ', at: ' + self.timeIns.get_hms() + ', id = ' + str(self.skelId) + ' containing: ' +  str(self.joints)
        return string

class Joint :
    def __init__(self, id, x, y, z) :
        self.id = id
        self.x = x
        self.y = y
        self.z = z

    def __repr__(self) :
        vals = [self.id, self.x, self.y, self.z]
        names = ['id', 'x', 'y', 'z']
        string = '<Joint at ' + str(hex(id(self)))
        for name, val in zip(names, vals) :
            string += ',' + name + '=' + str(val)
        string += '>'
        return string

class Skeleton :
    def __init__(self) :
        self.joints = [sphere(radius = 0.08, color = color.yellow) for i in range(20)]
        self.joints[3].radius = 0.125
        self.bones = [cylinder(radius = 0.05, color = color.yellow) for bone in _bone_ids]

    def update(self, frame) :
        for jointShape, jointObj in zip(self.joints, frame.joints) :
            joint.pos = (jointObj.x, jointObj.y, jointObj.z)

        for bone, bone_id in zip(self.bones, _bone_ids) :
            p1, p2 = [self.joints[id].pos for id in bone_id]
            bone.pos = p1
            bone.axis = p2 - p1
