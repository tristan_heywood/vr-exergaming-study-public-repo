from kinLog import Frame, Joint
import Tkinter as Tk
import matplotlib
matplotlib.use('TkAgg')
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.backend_bases import key_press_handler
from matplotlib.figure import Figure
from matplotlib.transforms import blended_transform_factory
from enum import Enum
from TimeInstance import TimeIns

class JID(Enum) :
    LEFT_HAND = 7
    RIGHT_HAND = 11
    LEFT_FOOT = 15
    RIGHT_FOOT = 19
    SPINE_SHOULDER = 20
    SPINE_BASE = 0

class GJoint :

    def __init__(self, xList, yList, zList) :
        self.xList = xList
        self.yList = yList
        self.zList = zList

    def flip_visible(self, canvas, comPipe) :
        for line in [self.lineX, self.lineY, self.lineZ] :
            line.set_visible(not line.get_visible())
        if comPipe is not None :
            if self.lineX.get_visible() :
                comPipe.selJoints.append(self.jid)
            else :
                if self.jid in comPipe.selJoints :
                    comPipe.selJoints.remove(self.jid)
        canvas.draw()

class Display :

    def __init__(self, person, comPipe = None) :
        print comPipe
        self.person = person
        self.comPipe = comPipe
        self.graph()

    def update_cur_time(self) :
        if self.comPipe.tMillis > self.person.timeArd0.millis :
            for v in [self.vertX, self.vertY, self.vertZ, self.vertHR] :
                v.set_xdata(self.comPipe.tMillis)
        self.canvas.draw()
        self.root.after(100, self.update_cur_time)

    def flip_visible(self, line) :
        if isinstance(line, list) :
            for obj in line :
                obj.set_visible(not obj.get_visible())
        else :
            line.set_visible(not line.get_visible())
        self.canvas.draw()

    def graph(self) :
        root = Tk.Tk()
        self.root = root
        gridSpec = matplotlib.gridspec.GridSpec(4, 1, height_ratios = [3, 3, 3, 5])
        fig = Figure(figsize = (12, 14), dpi = 60)
        #fig.tight_layout()
        self.axesX = fig.add_subplot(gridSpec[0])
        self.axesY = fig.add_subplot(gridSpec[1], sharex = self.axesX)
        self.axesZ = fig.add_subplot(gridSpec[2], sharex = self.axesX)
        self.axesHR = fig.add_subplot(gridSpec[3], sharex = self.axesX)

        joints = {}
        frames = self.person.kinFrames
        xAx = [f.timeIns.millis for f in frames]
        for jid in JID :
            jid = jid.value
            xList = [f.joints[jid].x for f in frames]
            yList = [f.joints[jid].y for f in frames]
            zList = [f.joints[jid].z for f in frames]
            joints[jid] = GJoint(xList, yList, zList)

        for jid, gJoint in joints.items() :
            gJoint.lineX = self.axesX.plot(xAx, gJoint.xList)[0]
            gJoint.lineY = self.axesY.plot(xAx, gJoint.yList)[0]
            gJoint.lineZ = self.axesZ.plot(xAx, gJoint.zList)[0]
            gJoint.jid = jid




        #self.axesX.plot(xAx, joints[JID.SPINE_BASE.value].xList)
        textX = self.axesX.set_title('X axis', loc = 'left')
        textX.set_color('r')
        textX.set_size('xx-large')
        textX.set_weight('bold')
        #self.axesY.plot(xAx, joints[JID.SPINE_BASE.value].yList)
        textY = self.axesY.set_title('Y axis', loc = 'left')
        textY.set_color('g')
        textY.set_size('xx-large')
        textY.set_weight('bold')
        #self.axesZ.plot(xAx, joints[JID.SPINE_BASE.value].zList)
        textZ = self.axesZ.set_title('Z axis', loc = 'left')
        textZ.set_color('b')
        textZ.set_size('xx-large')
        textZ.set_weight('bold')

        hrX = [d[0] + self.person.timeArd0.millis for d in self.person.bpmList]
        hrY = [d[1] for d in self.person.bpmList]

        self.axesHR.plot(hrX, hrY)
        self.axesHR.set_title('Heart Rate', loc = 'left')

        self.actAnots = []
        for act in (act for act in self.person.activities if act.needRest) :
            self.actAnots.append(self.axesHR.axvline(x = TimeIns.hms(act.bpmL[0][1]).millis, linewidth = 2, color = 'r'))
            self.actAnots.append(self.axesHR.axvline(x = TimeIns.hms(act.bpmL[-1][1]).millis, linewidth = 2, color = 'r'))
            trans = self.axesHR.get_xaxis_transform()
            self.actAnots.append(self.axesHR.annotate(act.name, xy = (TimeIns.hms(act.bpmL[0][1]).millis + 5000, 0.92), xycoords = trans, size = 18))

        self.modLine = self.axesHR.axhline(y = self.person.max_hr() * 0.5, linewidth = 2, color = 'y')
        self.vigLine = self.axesHR.axhline(y = self.person.max_hr() * 0.7, linewidth = 2, color = 'm')


        if self.comPipe is not None :
            root.after(100, self.update_cur_time)
            self.vertX = self.axesX.axvline(x = self.person.timeArd0.millis, linewidth = 3, color = 'k')
            self.vertY = self.axesY.axvline(x = self.person.timeArd0.millis, linewidth = 3, color = 'k')
            self.vertZ = self.axesZ.axvline(x = self.person.timeArd0.millis, linewidth = 3, color = 'k')
            self.vertHR = self.axesHR.axvline(x = self.person.timeArd0.millis, linewidth = 3, color = 'k')

        self.canvas = FigureCanvasTkAgg(fig, master = root)
        self.canvas.show()
        wig = self.canvas.get_tk_widget()

        frame = Tk.Frame(root)
        frame.pack(side = Tk.TOP, expand = 1, fill = Tk.BOTH)
        wig.pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)

        visGroups = [('Show Games', self.actAnots), ('Mod/Vig HR', [self.modLine, self.vigLine])]
        for ind, (name, group) in enumerate(visGroups) :
            but = Tk.Checkbutton(frame, text = name, command = lambda group = group : self.flip_visible(group))
            but.grid(row = 1, column = ind, sticky = 'W')
            but.select()

        for ind, jEnum in enumerate(JID) :
            gJoint = joints[jEnum.value]
            but = Tk.Checkbutton(frame, text = jEnum.name, command = lambda gJoint = gJoint : gJoint.flip_visible(self.canvas, self.comPipe))
            but.grid(row = 0, column = ind, sticky = 'W')
            gJoint.button = but
            gJoint.flip_visible(self.canvas, self.comPipe)

        joints[JID.SPINE_BASE.value].flip_visible(self.canvas, self.comPipe)
        joints[JID.SPINE_BASE.value].button.select()

        toolbar = NavigationToolbar2TkAgg(self.canvas, root)
        toolbar.update()
        self.canvas._tkcanvas.pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)
        Tk.mainloop()
