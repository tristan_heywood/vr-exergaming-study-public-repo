from __future__ import division
import openpyxl as xl
import re
import sys
import pdb
from TimeInstance import TimeIns
from difflib import SequenceMatcher
from pyLgVw import bpm_from_interval, HMS_to_ms
from LogStitcher import stitch_logs
from timestampFixer import fix_timestamps
import cPickle as pickle
import os
import BpmAlg
from collections import OrderedDict

class Person :

    #A = fruit, B = holo, C = squat, D = portal
    def __init__(self, name = None, activities = []) :
        self.name = name
        self.gameOrder = None
        self.activities = activities
        self.actDict = {}
        self.BpmList = None

    def convert_game_order(self) :
        try :
            self.gameOrder = self.gameOrder.replace('A', 'F')
            self.gameOrder = self.gameOrder.replace('B', 'H')
            self.gameOrder = self.gameOrder.replace('C', 'S')
            self.gameOrder = self.gameOrder.replace('D', 'P')
        except AttributeError :
            pass

    #https://www.ncbi.nlm.nih.gov/pubmed/17468581
    def max_hr(self) :
        #median is 26
        return 220 - 26

    def smooth_hr(self) :
        out = []
        time = self.bpmList[2][0]
        lowInd = 0
        highInd = 0
        while time + 15000 < self.bpmList[-1][0] :
            while self.bpmList[lowInd][0] < time - 15000 : lowInd += 1
            while self.bpmList[highInd][0] < time + 15000 : highInd += 1
            window = self.bpmList[lowInd : highInd]
            #window = [x for x in self.bpmList if x[0] >= time - 7500 and x[0] <= time + 15000]
            if len(window) == 0 :
                time += 250
                continue
            avg = sum(x[1] for x in window) / len(window)
            out.append((time, avg))
            time += 250
        return out

    def resting_hr(self) :
        mini = sys.maxint
        for startTs, startBpm in (x for x in self.bpmList if self.bpmList[-1][0] - x[0] >= 60000 * 2) :
            relev = [x for x in self.bpmList if x[0] >= startTs and x[0] <= startTs + 60000 * 2]
            if len(relev) < 20 : continue #don't count if there's not enough data points
            avg = sum(x[1] for x in relev) // len(relev)
            mini = min(mini, avg)
        return mini

    def setup_actDict(self) :
        self.actDict = {act.name : act for act in self.activities}

    def add_smoothed(self, xfile, yfile) :
        with open(xfile, 'r') as xf, open(yfile, 'r') as yf :
            xList = [int(float(x[:-1])) for x in xf]
            yList = [int(float(y[:-1])) for y in yf]
            xList = xList[:min(len(xList), len(yList))]
            yList = yList[:min(len(xList), len(yList))]
            self.smoothedX = xList
            self.smoothedY = yList

    def time(self, acName) :
        if acName not in self.actDict.keys() :
            return ''
        return self.actDict[acName].time()

    def kinFrames_to_disk(self) :
        fname = '' + self.name + '.pkl'
        with open(fname, 'wb') as outF :
            try :
                pickle.dump(self.kinFrames, outF, pickle.HIGHEST_PROTOCOL)
                del self.kinFrames
            except AttributeError :
                print 'error: ' + self.name + ' has no kinFrames list'

    def kinFrames_from_disk(self) :
        if hasattr(self, 'kinFrames') :
            return
        fname = '' + self.name + '.pkl'
        try :
            with open(fname, 'rb') as inF :
                self.kinFrames = pickle.load(inF)
                print self.name + ' loaded kinFrame list of length: ' + str(len(self.kinFrames))
        except IOError :
            print 'error: could not find kinFrame file for ' + self.name

    def __str__(self) :
        outStr = self.name + ', ' + self.gameOrder + '\n'
        for act in self.activities :
            outStr += '\t' + str(act) + '\n'
        return outStr

    def allocate_rests(self) :
        #for each activity that is a rest
        for act in [act for act in self.activities if 'rest' in act.name.lower()] :
            minDist = sys.maxint
            minAct = None
            #Search through all activities that need a rest
            for game in [game for game in self.activities if game.needRest] :
                #find the one that occurs soonest after act
                dist = game.end.dist_to(act.begin)
                if dist >= 0 and dist < minDist :
                    minDist = dist
                    minAct = game
            if minAct is not None :
                #if this is the first rest after this game found or if this rest is closer than a previously found rest
                if minAct.acRest is None or minDist < minAct.end.dist_to(act.begin) :
                    minAct.acRest = act
                    act.name = minAct.name + ' Rest'


class Partic :
    def __init__(self, name, title) :
        self.name = name
        self.title = title
        self.timesDict = {}

class Activity :

    def __init__(self, name, begin, end) :
        self.name = name
        self.begin = begin
        self.end = end
        self.acRest = None
        self.bpmL = None
        self.aveHR = None
        self.maxHR = None
        self.sheetBpmL = None
        self.needRest = False

        if 'fruit' in self.name.lower() :
            self.name = 'Fruit Ninja'
            self.needRest = True
        elif 'holo' in self.name.lower() :
            self.name = 'Holopoint'
            self.needRest = True
        elif 'squat' in self.name.lower() :
            self.name = 'Squat'
            self.needRest = True

    def time(self) :
        return self.begin.diff_mins(self.end)

    def rest_time(self) :
        if self.acRest is None :
            return ''
        return self.acRest.time()

    def average_hr(self) :
        return sum(x[2] for x in self.bpmL) // len(self.bpmL)

    def max_hr(self) :
        return int(max(x[2] for x in self.bpmL))

    def play_time(self) :
        return self.begin.diff_mins(self.end)

    def rest_hr(self, minStart) :
        if self.end.millis - self.begin.millis < 180000 : return None #if there isn't three minutes of rest
        relPoints = [x[2] for x in self.bpmL if x[0] >= self.bpmL[0][0] + 60000 * minStart and x[0] <= self.bpmL[0][0] + 60000 * (minStart + 1)]
        return sum(relPoints) // len(relPoints)

    #counts the number of 10 second blocks averaging at least the hrCutoff
    def blocks_at_frac(self, frac) :
        hrCutoff = self.person.max_hr() * frac
        prev = self.bpmL[0][0]
        time = 0
        while self.bpmL[-1][0] - prev > 10000 :
            relev = [x for x in self.bpmL if x[0] >= prev and x[0] <= prev + 10000]
            if sum(x[2] for x in relev) // len(relev) >= hrCutoff :
                prev = relev[-1][0]
                time += relev[-1][0] - relev[0][0]
            else : prev = next(x for x in self.bpmL if x[0] > prev)[0]
        return '0:{:02d}:{:02d}'.format(*divmod(time // 1000, 60))

    def final_min_ave(self) :
        relevPoints = [x for x in self.bpmL if self.bpmL[-1][0] - x[0] < 60000]
        return sum(x[2] for x in relevPoints) // len(relevPoints)

    def max_over_millis(self, millis) :
        maxSeen = -1

        for ts, hms, bpm in self.bpmL :
            if ts + millis > self.bpmL[-1][0] :
                return maxSeen
            relevPoints = [x for x in self.bpmL if x[0] >= ts and x[0] <= ts + millis]
            maxSeen = max(maxSeen, sum(x[2] for x in relevPoints) // len(relevPoints))

    def test_meth(self) :
        return 4

    def get_max_hr(self) :
        if self.maxHR is None :
            self.maxHR = int(max(self.bpmL, key = lambda t : t[2])[2])
        return self.maxHR

    def get_max_over_secs(self, seconds) :
        maximum = -1
        dur = seconds * 1000 #duration in millis

        for ts, hms, bpm in self.bpmL :
            if ts + dur > self.end.millis :
                return curMax
            relPoints = [x[2] for x in self.bpmL if x[0] >= ts and x[0] <= ts + dur]
            aveHr = sum(relPoints) // len(relPoints)
            maximum = max(maximum, aveHr)

    #repr can recurse infintely - that should make debugging fun
    def __repr__(self) :
        return str(id(self)) + ', ' + self.name + ' from ' + self.begin.get_hms() + ' to ' + self.end.get_hms() + ('. with rest: ' + repr(self.acRest) if self.acRest is not None else '')

    def __str__(self) :
        return self.name + ' from ' + self.begin.get_hms() + ' to ' + self.end.get_hms()

class OrderGroup :

    def __init__(self, name, people) :
        self.name = name
        self.people = people

    def __str__(self) :
        return self.name + ', ' + str([p.name for p in self.people])
def timetable_to_personObj_list(fname) :
    study = open(fname, 'r')
    people = []
    line = study.readline()
    while len(line) != 0 :
        p = Person()
        p.activities = []
        p.name, p.gameOrder = line.split()[0:2]
        line = study.readline()
        while not line.isspace() and len(line) != 0:
            name = line.strip()
            line = study.readline()
            tups = re.findall(r'(\d+:\d+:\d+)', line)
            start = TimeIns.hms(tups[0])
            end = TimeIns.hms(tups[1])
            act = Activity(name, start, end)
            p.activities.append(act)
            line = study.readline()
        people.append(p)

        while line.isspace() :
            line = study.readline()

    return people

def create_summary_sheet(workbookObj, people, peopleOrder, sheetName, method, actOrder = ['Tutorial', 'Squat', 'Squat Rest', 'Fruit Ninja', 'Fruit Ninja Rest', 'Holopoint', 'Holopoint Rest', 'Portal']) :
    ws = workbookObj.create_sheet(sheetName)

#Takes list of the form [(ts, bpm)...] and interpolates to give at least one bpm measurement per second
def interpolate_denser_bpm(bpmL) :
    newBpm = []
    for ind, (ts, bpm) in enumerate(bpmL) :
        if ind == 0 :
            newBpm.append((ts, bpm))
        prevTime = bpmL[ind - 1][0]
        while ts - prevTime > 1000 :
            t = prevTime + 1000
            distPrev = abs(t - bpmL[ind - 1][0])
            distNext = abs(t - ts)
            wAve = distNext * bpmL[ind - 1][1] + distPrev * bpm
            wAve /= (distPrev + distNext)
            newBpm.append((t, wAve))
            prevTime += 1000
        newBpm.append((ts, bpm))
    return newBpm

if __name__ == '__main__' :
    # rb = xl.Workbook()
    # ws = rb.active()

    people = timetable_to_personObj_list('timetable (1).txt')

    os.chdir('./study2')

    wb = xl.Workbook()
    ws = wb.active

    for person in people :
        print 'processing', person.name
        fname = [fname for fname in os.listdir('./') if person.name.lower() in fname.lower()][0] #there must be a cleaner way
        hrFile = open(fname, 'r')
        fileStr = hrFile.read()
        tuples = re.findall(r'(\d{2}:\d{2}\:\d{2}).\d+ Band Beat: (\d+)', fileStr)
        beats = []

        hmsStart = tuples[0][0]
        tsStart = tuples[1][1]
        beats = [int(t[1]) for t in tuples]
        bpmL = BpmAlg.calc_bpm(beats)[3]

        print '\tBpmAlg complete'

        newBpm = interpolate_denser_bpm(bpmL)

        hmsStart = TimeIns.hms(hmsStart)
        tsStart = int(tsStart)

        for act in person.activities :
            start = act.begin - hmsStart #minus is overloaded
            ind = 0
            relBpm = []

            while newBpm[ind][0] - tsStart < start :
                ind += 1

            stop = act.end - hmsStart
            while ind < len(newBpm) and newBpm[ind][0] - tsStart < stop :
                relBpm.append((newBpm[ind][0], TimeIns(newBpm[ind][0] - tsStart + hmsStart.millis).get_hms(), newBpm[ind][1]))
                ind += 1

            act.bpmL = relBpm

        restGames = []
        for act in person.activities :
            if 'fruit' in act.name.lower() :
                act.name = 'Fruit Ninja'
                restGames.append(act)
            elif 'holo' in act.name.lower() :
                act.name = 'Holopoint'
                restGames.append(act)
            elif 'Squat' == act.name :
                restGames.append(act)
            person.actDict[act.name.lower()] = act

        #for all rest activities
        for act in person.activities : #[act in person.activities if 'rest' in act.name] :
            if 'rest' not in act.name.lower() or '4' in act.name:
                continue
            minDist = sys.maxint
            minAct = None
            #for all games that need rest afterwards
            for game in restGames :
                #find the game which ended closest to the start of this rest but still happened before this rest
                dist = game.end.dist_to(act.begin)
                if dist >= 0 and dist < minDist :
                    minDist = dist
                    minAct = game
            if minAct is not None :
                if minAct.acRest is not None :
                    print 'ERROR: OVERWRITNG REST'
                    pdb.set_trace()
                minAct.acRest = act

                act.name = minAct.name + ' Rest'



        ws = wb.create_sheet(person.name)

        ws.cell(row = 1, column = 1).value = person.name + ', ' + person.gameOrder
        ind = 2
        for act in person.activities :
            ws.cell(row = ind, column = 1).value = act.name + ', ' + act.begin.get_hms() + ' - ' + act.end.get_hms()
            ind += 1
            last = 0
            act.sheetBpmL = []
            for ts, hms, bpm in act.bpmL :
                if ts - last > 2000 : #only add a new number every 2 seconds
                    ws.cell(row = ind, column = 1).value = hms
                    ws.cell(row = ind, column = 2).value = int(bpm)
                    act.sheetBpmL.append((ts, int(bpm)))
                    ind += 1
                    last = ts
        ws['G18'] = 'Summary HR'
        ws['H19'] = 'MAX'
        ws['I19'] = 'MIN'
        ws['J19'] = 'AVG'
        ws['K19'] = 'AVG final min'
        ws['L19'] = 'TIME (mins)'

        curRow = 20
        useWave = False
        for act in person.activities :
            # if len(act.bpmL) == 0 :
            #     pdb.set_trace()
            ws.cell(row = curRow, column = 7).value = act.name
            act.maxHR = int(max(act.bpmL, key = lambda t : t[2])[2])
            ws.cell(row = curRow, column = 8).value = act.maxHR
            act.minHR = int(min(act.bpmL, key = lambda t : t[2])[2])
            ws.cell(row = curRow, column = 9).value = act.minHR

            if useWave :
                Wave = 0
                prevTs = 0
                for ts, hms, bpm in act.bpmL :
                    if prevTs == 0 :
                        prevTs = ts
                        continue
                    Wave += (ts - prevTs) * bpm
                    prevTs = ts
                Wave /= (act.bpmL[-1][0] - act.bpmL[0][0])
                Wave = int(Wave)
                act.aveHR = Wave
                ws.cell(row = curRow, column = 10).value = Wave
            else :
                act.aveHR = sum(x[1] for x in act.sheetBpmL) // len(act.sheetBpmL)
                ws.cell(row = curRow, column = 10).value = act.aveHR

            lastTs = act.sheetBpmL[-1][0]
            lastMin = [x for x in act.sheetBpmL if x[0] >= lastTs - 60000]
            lastMinAve = sum(x[1] for x in lastMin) // len(lastMin)
            act.lastMinAve = lastMinAve
            ws.cell(row = curRow, column = 11).value = lastMinAve
            ws.cell(row = curRow, column = 12).value = act.end.diff_mins(act.begin)

            curRow += 1



    ws = wb.create_sheet('Average HR')
    actOrder = ['Tutorial', 'Squat', 'Squat Rest', 'Fruit Ninja', 'Fruit Ninja Rest', 'Holopoint', 'Holopoint Rest', 'Portal']
    ws['A2'] = 'Tutorial'
    ws['A3'] = 'Squat'
    ws['A4'] = 'Squat Rest'
    ws['A5'] = 'Fruit Ninja'
    ws['A6'] = 'Fruit Ninja Rest'
    ws['A7'] = 'Holopoint'
    ws['A8'] = 'Holopoint Rest'
    ws['A9'] = 'Portal'
    curCol = 2
    for name in order :
        person = [p for p in people if p.name.lower() == name.lower()][0]
        curRow = 1
        ws.cell(row = curRow, column=curCol).value = person.name
        curRow += 1
        for actName in actOrder :
            act = [act for act in person.activities if act.name.lower() == actName.lower()]
            if len(act) != 1 :
                curRow += 1
                continue
            act = act[0]
            if act.aveHR is None :
                curRow += 1
                continue
            ws.cell(row = curRow, column=curCol).value = act.aveHR
            curRow += 1
        curCol += 1

    ws = wb.create_sheet('Max HR')
    ws['A2'] = 'Tutorial'
    ws['A3'] = 'Squat'
    ws['A4'] = 'Squat Rest'
    ws['A5'] = 'Fruit Ninja'
    ws['A6'] = 'Fruit Ninja Rest'
    ws['A7'] = 'Holopoint'
    ws['A8'] = 'Holopoint Rest'
    ws['A9'] = 'Portal'
    curCol = 2
    for name in order :
        person = [p for p in people if p.name.lower() == name.lower()][0]
        curRow = 1
        ws.cell(row = curRow, column=curCol).value = person.name
        curRow += 1
        for actName in actOrder :
            act = [act for act in person.activities if act.name.lower() == actName.lower()]
            if len(act) != 1 :
                curRow += 1
                continue
            act = act[0]
            if act.maxHR is None :
                curRow += 1
                continue
            ws.cell(row = curRow, column=curCol).value = act.maxHR
            curRow += 1
        curCol += 1

    ws = wb.create_sheet('Final Min')
    ws['A2'] = 'Tutorial'
    ws['A3'] = 'Squat'
    ws['A4'] = 'Squat Rest'
    ws['A5'] = 'Fruit Ninja'
    ws['A6'] = 'Fruit Ninja Rest'
    ws['A7'] = 'Holopoint'
    ws['A8'] = 'Holopoint Rest'
    ws['A9'] = 'Portal'
    curCol = 2
    for name in order :
        person = [p for p in people if p.name.lower() == name.lower()][0]
        curRow = 1
        ws.cell(row = curRow, column=curCol).value = person.name
        curRow += 1
        for actName in actOrder :
            act = [act for act in person.activities if act.name.lower() == actName.lower()]
            if len(act) != 1 :
                curRow += 1
                continue
            act = act[0]
            if act.maxHR is None :
                curRow += 1
                continue
            try :
                ws.cell(row = curRow, column=curCol).value = act.lastMinAve
            except AttributeError :
                print p.name + ', ' + act.name + ' missing lastMinAve'
            curRow += 1
        curCol += 1


    ws = wb.create_sheet('Max10')
    ws['A2'] = 'Tutorial'
    ws['A3'] = 'Squat'
    ws['A4'] = 'Squat Rest'
    ws['A5'] = 'Fruit Ninja'
    ws['A6'] = 'Fruit Ninja Rest'
    ws['A7'] = 'Holopoint'
    ws['A8'] = 'Holopoint Rest'
    ws['A9'] = 'Portal'
    curCol = 2
    for name in order :
        person = [p for p in people if p.name.lower() == name.lower()][0]
        curRow = 1
        ws.cell(row = curRow, column=curCol).value = person.name
        curRow += 1
        for actName in actOrder :
            act = [act for act in person.activities if act.name.lower() == actName.lower()]
            if len(act) != 1 :
                curRow += 1
                continue
            act = act[0]

            pdb.set_trace()
            ws.cell(row = curRow, column=curCol).value = act.get_max_over_secs(10)
            curRow += 1
        curCol += 1


    os.chdir('..')
    wb.save('study2NewMax10.xlsx')

    sys.exit(0)



    for sheet in [sheet for sheet in rb if sheet.title in names] :

        print sheet.title
        yCo = 1
        activities = []
        while '*' not in str(sheet.cell(column=1, row=yCo).value) :

            if str(sheet.cell(column=1, row=yCo).value).count(':') > 3 :

                acName = sheet['A' + str(yCo)].value.split(',')[0]
                while sheet.cell(column = 2, row = yCo).value is None :
                    yCo += 1

                acBegin = TimeIns.hms(sheet['A' + str(yCo)].value)

                lastT = None
                while str(sheet.cell(column=1, row=yCo).value).count(':') < 3 and '*' not in str(sheet.cell(column=1, row=yCo).value):
                    if sheet.cell(column = 2, row = yCo).value is not None :
                        lastT = sheet.cell(column = 1, row = yCo).value
                        if lastT is None :
                            pdb.set_trace()
                    yCo += 1

                acEnd = TimeIns.hms(lastT)

                activities.append(Activity(acName, acBegin, acEnd))
                print repr(activities[-1])
            else :
                yCo += 1


        activities.sort(key = lambda act : act.begin.millis)
        people.append(Person(sheet.title, activities))

    for person in people :
        restGames = []
        for act in person.activities :
            if 'fruit' in act.name.lower() :
                act.name = 'Fruit Ninja'
                restGames.append(act)
            elif 'holo' in act.name.lower() :
                act.name = 'Holopoint'
                restGames.append(act)
            elif 'Squat' == act.name :
                restGames.append(act)
            person.actDict[act.name.lower()] = act


        #for all rest activities
        for act in person.activities : #[act in person.activities if 'rest' in act.name] :
            if 'rest' not in act.name.lower() :
                continue
            minDist = sys.maxint
            minAct = None
            #for all games that need rest afterwards
            for game in restGames :
                #find the game which ended closest to the start of this rest but still happened before this rest
                dist = game.end.dist_to(act.begin)
                if dist >= 0 and dist < minDist :
                    minDist = dist
                    minAct = game
            if minAct is not None :
                if minAct.acRest is not None :
                    print 'ERROR: OVERWRITNG REST'
                    pdb.set_trace()
                minAct.acRest = act

                act.name = minAct.name + ' Rest'


        print person.name
        for act in person.activities :
            print repr(act)

    os.chdir('./HR')
    direc = os.listdir('./')

    for fname in direc :
        if '.txt' not in fname and not os.path.isdir(fname) : continue
        print 'processing', fname
        match = re.search('Log_(\w+).txt', fname)
        if match: name = match.group(1)
        else : name = fname
        p = [p for p in people if p.name.lower() == name.lower()]
        if len(p) == 0 :
            #probably the worst way anyone has even fixed a typo
            p = max([(x, SequenceMatcher(None, x.name, name).ratio()) for x in partics], key=lambda x:x[1])[0]
        else : p = p[0]

        if os.path.isdir(fname) :
            path = os.getcwd() + '\\' + fname
            os.chdir(path)
            direc = os.listdir('./')
            stitch_logs(*[open(f, 'r') for f in direc if '.txt' in f and 'STITCHED' not in f])
            fname = 'HR_LOG_STITCHED.txt'

        hrFile = open(fname, 'r')
        fileStr = fix_timestamps(hrFile.read())

        tuples = re.findall(r'(\d{2}:\d{2}\:\d{2}).\d+ Band Beat: (\d+)', fileStr)

        beats = []
        hmsStart = tuples[0][0]
        tsStart = tuples[1][1]
        #time_dict = OrderedDict()
        #ts_to_HMS_dict = {}
        for t in tuples :
            beats.append(int(t[1]))
            #time_dict[t[0]] = int(t[1])
            #ts_to_HMS_dict[int(t[1])] = t[0]

        bpmL = BpmAlg.calc_bpm(beats)[3]
        newBpm = []

        #interpolate to give at least one bpm measurment per second
        for ind, (ts, bpm) in enumerate(bpmL) :
            if ind == 0 :
                newBpm.append((ts, bpm))
            prevTime = bpmL[ind - 1][0]
            while ts - prevTime > 1000 :
                t = prevTime + 1000
                distPrev = abs(t - bpmL[ind - 1][0])
                distNext = abs(t - ts)
                wAve = distNext * bpmL[ind - 1][1] + distPrev * bpm
                wAve /= (distPrev + distNext)
                newBpm.append((t, wAve))
                prevTime += 1000
            newBpm.append((ts, bpm))

        #uncomment to convince yourself that the interpolation works
        # import matplotlib
        # matplotlib.pyplot.plot([x[0] for x in bpmL], [x[1] for x in bpmL], [x[0] for x in newBpm], [x[1] for x in newBpm])
        # matplotlib.pyplot.show()

        print os.getcwd(), fname, p.name

        ind = 0
        hmsStart = TimeIns.hms(hmsStart).millis
        tsStart = int(tsStart)
        for act in p.activities :
            start = act.begin.millis - hmsStart
            relBpm = []

            while newBpm[ind][0] - tsStart < start :
                ind += 1

            #newBpm[ind] is now roughtly at start

            stop = act.end.millis - hmsStart
            while ind < len(newBpm) and newBpm[ind][0] - tsStart < stop :
                relBpm.append((newBpm[ind][0], TimeIns(newBpm[ind][0] - tsStart + hmsStart).get_hms(), newBpm[ind][1]))
                ind += 1

            act.bpmL = relBpm

        #p.timesDict = {game : (start, finish, bpm_from_interval(fname, start, finish)) for game, (start, finish) in p.timesDict.items()}
        if 'STITCHED' in fname : os.chdir('..')
    wb = xl.Workbook()
    ws = wb.active

    for person in people :
        ws = wb.create_sheet(person.name)
        ws.cell(row = 1, column = 1).value = person.name
        ind = 2
        for act in person.activities :
            ws.cell(row = ind, column = 1).value = act.name + ', ' + act.begin.get_hms() + ' - ' + act.end.get_hms()
            ind += 1
            last = 0
            act.sheetBpmL = []
            for ts, hms, bpm in act.bpmL :
                if ts - last > 2000 : #only add a new number every 2 seconds
                    ws.cell(row = ind, column = 1).value = hms
                    ws.cell(row = ind, column = 2).value = int(bpm)
                    act.sheetBpmL.append((ts, int(bpm)))
                    ind += 1
                    last = ts
        ws['G18'] = 'Summary HR'
        ws['H19'] = 'MAX'
        ws['I19'] = 'MIN'
        ws['J19'] = 'AVG'
        ws['K19'] = 'AVG final min'

        curRow = 20
        useWave = False
        for act in person.activities :
            # if len(act.bpmL) == 0 :
            #     pdb.set_trace()
            ws.cell(row = curRow, column = 7).value = act.name
            act.maxHR = int(max(act.bpmL, key = lambda t : t[2])[2])
            ws.cell(row = curRow, column = 8).value = act.maxHR
            act.minHR = int(min(act.bpmL, key = lambda t : t[2])[2])
            ws.cell(row = curRow, column = 9).value = act.minHR

            if useWave :
                Wave = 0
                prevTs = 0
                for ts, hms, bpm in act.bpmL :
                    if prevTs == 0 :
                        prevTs = ts
                        continue
                    Wave += (ts - prevTs) * bpm
                    prevTs = ts
                Wave /= (act.bpmL[-1][0] - act.bpmL[0][0])
                Wave = int(Wave)
                act.aveHR = Wave
                ws.cell(row = curRow, column = 10).value = Wave
            else :
                act.aveHR = sum(x[1] for x in act.sheetBpmL) // len(act.sheetBpmL)
                ws.cell(row = curRow, column = 10).value = act.aveHR

            lastTs = act.sheetBpmL[-1][0]
            lastMin = [x for x in act.sheetBpmL if x[0] >= lastTs - 60000]
            lastMinAve = sum(x[1] for x in lastMin) // len(lastMin)
            act.lastMinAve = lastMinAve
            ws.cell(row = curRow, column = 11).value = lastMinAve

            curRow += 1

    order = ['']
    ws = wb.create_sheet('Average HR')
    actOrder = ['Tutorial', 'Squat', 'Squat Rest', 'Fruit Ninja', 'Fruit Ninja Rest', 'Holopoint', 'Holopoint Rest', 'Portal']
    ws['A2'] = 'Tutorial'
    ws['A3'] = 'Squat'
    ws['A4'] = 'Squat Rest'
    ws['A5'] = 'Fruit Ninja'
    ws['A6'] = 'Fruit Ninja Rest'
    ws['A7'] = 'Holopoint'
    ws['A8'] = 'Holopoint Rest'
    ws['A9'] = 'Portal'
    curCol = 2
    for name in order :
        person = [p for p in people if p.name.lower() == name.lower()][0]
        curRow = 1
        ws.cell(row = curRow, column=curCol).value = person.name
        curRow += 1
        for actName in actOrder :
            act = [act for act in person.activities if act.name.lower() == actName.lower()]
            if len(act) != 1 :
                curRow += 1
                continue
            act = act[0]
            if act.aveHR is None :
                curRow += 1
                continue
            ws.cell(row = curRow, column=curCol).value = act.aveHR
            curRow += 1
        curCol += 1

    ws = wb.create_sheet('Max HR')
    ws['A2'] = 'Tutorial'
    ws['A3'] = 'Squat'
    ws['A4'] = 'Squat Rest'
    ws['A5'] = 'Fruit Ninja'
    ws['A6'] = 'Fruit Ninja Rest'
    ws['A7'] = 'Holopoint'
    ws['A8'] = 'Holopoint Rest'
    ws['A9'] = 'Portal'
    curCol = 2
    for name in order :
        person = [p for p in people if p.name.lower() == name.lower()][0]
        curRow = 1
        ws.cell(row = curRow, column=curCol).value = person.name
        curRow += 1
        for actName in actOrder :
            act = [act for act in person.activities if act.name.lower() == actName.lower()]
            if len(act) != 1 :
                curRow += 1
                continue
            act = act[0]
            if act.maxHR is None :
                curRow += 1
                continue
            ws.cell(row = curRow, column=curCol).value = act.maxHR
            curRow += 1
        curCol += 1

    ws = wb.create_sheet('Final Min')
    ws['A2'] = 'Tutorial'
    ws['A3'] = 'Squat'
    ws['A4'] = 'Squat Rest'
    ws['A5'] = 'Fruit Ninja'
    ws['A6'] = 'Fruit Ninja Rest'
    ws['A7'] = 'Holopoint'
    ws['A8'] = 'Holopoint Rest'
    ws['A9'] = 'Portal'
    curCol = 2
    for name in order :
        person = [p for p in people if p.name.lower() == name.lower()][0]
        curRow = 1
        ws.cell(row = curRow, column=curCol).value = person.name
        curRow += 1
        for actName in actOrder :
            act = [act for act in person.activities if act.name.lower() == actName.lower()]
            if len(act) != 1 :
                curRow += 1
                continue
            act = act[0]
            if act.maxHR is None :
                curRow += 1
                continue
            try :
                ws.cell(row = curRow, column=curCol).value = act.lastMinAve
            except AttributeError :
                print p.name + ', ' + act.name + ' missing lastMinAve'
            curRow += 1
        curCol += 1


    os.chdir('..')
    wb.save('newMeth.xlsx')



