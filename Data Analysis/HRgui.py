#A triumph of guess and check coding
from pyLgVw import *
from Tkinter import *
import tkFileDialog
import matplotlib
import BpmAlg
matplotlib.use('TkAgg')

from numpy import arange, sin, pi
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
# implement the default mpl key bindings
from matplotlib.backend_bases import key_press_handler


from matplotlib.figure import Figure

import sys
if sys.version_info[0] < 3:
    import Tkinter as Tk
else:
    import tkinter as Tk

band_beats = []
ear_beats =[]
wrist_bpm  =[]

plots = []

### start copied code

root = Tk.Tk()
root.wm_title("Embedding in TK")

gridSpec = matplotlib.gridspec.GridSpec(2, 1, height_ratios=[7, 1])

f = Figure(figsize=(12, 12), dpi=60)
axes = f.add_subplot(gridSpec[0]) #211, 111

#axes.plot(t, s)

axes2 = f.add_subplot(gridSpec[1], sharex=axes) #212
#matplotlib.pyplot.tight_layout()

# a tk.DrawingArea
canvas = FigureCanvasTkAgg(f, master=root)
canvas.show()
wig = canvas.get_tk_widget()

###end copied code


class Plot :
    def __init__(self, id, chkbox, params=(2, 1.5, 1)) :
        self.name = id + ':' + str(params)
        self.chkOn = True
        print self.name, 'created'
        beatslist = None
        bpmlist = None

        if id == 'band' : beatslist = band_beats
        elif id == 'ear' : beatslist = ear_beats
        elif id == 'wrist' : bpmlist = wrist_bpm
        else : print 'ERROR in plot init, invalid id'

        '''
        if bpmlist is None :
            bpmlist = beats_to_bpm(beatslist, params[0], params[1])
        bpmlist = filter(bpmlist, params[2])
        '''

        # if filtVar.get() == 'Raw Beats' :
        #     # lines = [[(x, 0), (x, 200)] for x in beatslist]
        #     # lineCol = matplotlib.collections.LineCollection(lines, linewidths=2)
        #     # self.line = axes.add_collection(lineCol)
        #     # axes.autoscale()
        #     self.line = axes.scatter(beatslist, [0] * len(beatslist))
        #     self.chkbox = chkbox

        if bpmlist is None :
            global filtVar
            if filtVar.get() == 'Smart Filter' :
                bpmlist = smart_filter(beatslist, params[0], params[1], params[2] )
            elif filtVar.get() == 'Filter Bpm' :
                bpmlist = beats_to_bpm(beatslist, params[0], params[1])
                bpmlist = filter(bpmlist, params[2])
            elif filtVar.get() == 'Raw Bpm' :
                bpmlist = raw_bpm(beatslist)
            else :
                print 'ERROR: invalid value for filtVar (' + filtVar.get() + ')'

        (self.xlist, self.ylist) = ([x[0] for x in bpmlist], [x[1] for x in bpmlist])
        self.chkbox = chkbox
        self.line = axes.plot(self.xlist, self.ylist)[0]

        self.beatPlot = BeatPlot(beatslist, axes2)
        (self.algX, self.algY) = ([x[0] for x in self.beatPlot.bpmL], [x[1] for x in self.beatPlot.bpmL])

        self.algLine = axes.plot(self.algX, self.algY)[0]
        # self.beatsLine = axes2.scatter(beatslist, [0] * len(beatslist), marker='.')
        # self.wsBeatsList = BpmAlg.calc_bpm(beatslist)
        # self.wsBeatsLine = axes2.scatter(self.wsBeatsList, [300] * len(self.wsBeatsList), marker='.')

        #e.g. creating a new band plot will turn all others off

        for p in plots :
            if id in p.name and p.chkOn :
                p.chkbox.deselect()
                onCheckChange(p)

        #chkbox.config(highlightbackground='blue')
        canvas.draw()
    def killself(self) :
        self.line.remove()
        self.beatPlot.remove()
        # self.beatsLine.remove()
        # self.wsBeatsLine.remove()
        self.chkbox.grid_remove()
        self.algLine.remove()

#class encapsulating the beat scatter plot under the line graph
class BeatPlot :

    def __init__(self, beatsList, axes) :
        self.mainPlot = axes.scatter(beatsList, [0] * len(beatsList), marker='.')
        wsBeatsList, divList, algBeats, self.bpmL = BpmAlg.calc_bpm(beatsList)
        self.wsPlot = axes.scatter(wsBeatsList, [300] * len(wsBeatsList), marker = '.')
        self.divPlot = axes.scatter(divList, [250] * len(divList))
        self.algPlot = axes.scatter(algBeats, [200] * len(algBeats), marker = '.')
    def remove(self) :
        self.mainPlot.remove()
        self.wsPlot.remove()
        self.divPlot.remove()
        self.algPlot.remove()
    def toggle_vis(self) :
        self.mainPlot.set_visible(not self.mainPlot.get_visible())
        self.wsPlot.set_visible(not self.wsPlot.get_visible())
        self.divPlot.set_visible(not self.divPlot.get_visible())
        self.algPlot.set_visible(not self.algPlot.get_visible())

def onEntry(boxname) :
    print boxname, 'changed'
def onLogPicked() :
    print var.get(), 'chosen'
    set_active_log(var.get())

def onFiltPicked() :
    print filtVar.get(), 'chosen'


def onButton(id) :
    print id, 'pressed'
    params = re.findall(r'(\d+\.?\d*)', entryDict[id].get())
    paramsTup = (2, 1.5, 1)
    if len(params) == 3 : paramsTup = (float(params[0]), float(params[1]), float(params[2]))
    else : print 'Wrong number of paramaters, using defualts'
    print params

    for butName in entryDict.keys() :
        if id == butName or id == 'master' :

            if filtVar.get() == 'Raw Bpm' : butTxt = 'Raw Bpm'
            if filtVar.get() == 'Raw Beats' : butTxt = 'Raw Beats'
            else : butTxt = str(paramsTup) + (' Smart' if filtVar.get() == 'Smart Filter' else ' BPM')

            ckbut = Tk.Checkbutton(frame, text = butTxt)
            ckbut.grid(row = rowDict[butName], column = colDict[butName])
            colDict[butName] += 1
            global plots
            plots.append(Plot(id, ckbut, paramsTup))
            ckbut.select()
            ckbut.config(command = lambda x = plots[-1] : onCheckChange(x))

            #var.trace('w', lambda name, index, mode, x = plots[-1] : onCheckChange(x))


def onCheckChange(plot) :

    plot.chkOn = not plot.chkOn
    print plot.name, 'status:', plot.chkOn
    plot.line.set_visible(not plot.line.get_visible())
    plot.algLine.set_visible(not plot.algLine.get_visible())
    plot.beatPlot.toggle_vis()
    # plot.beatsLine.set_visible(not plot.beatsLine.get_visible())
    # plot.wsBeatsLine.set_visible(not plot.wsBeatsLine.get_visible())

    canvas.draw()

    '''
    global activePlots

    if plot.chkOn : activePlots.append(plot)
    else : activePlots.remove(plot)

    xylist = []
    for ap in activePlots :
        xylist.append(ap.xlist)
        xylist.append(ap.ylist)
        print ap.name, 'is active'
    axes.plot(*xylist)
    canvas.draw()
    '''

def onMasterEntryChange(mastVar) :
    text = mastVar.get()
    print text
    for entry in entryDict.values() :
        entry.delete(0, END)
        entry.insert(0, text)

def onOpenPicked() :
    fname = tkFileDialog.askopenfilename()
    print 'user chose file', fname,
    set_active_log(fname)

    logs.append(fname)
    logMenu.addOptions(logs)

def onOpenFolderPicked() :
    dirStr =  tkFileDialog.askdirectory()
    global logMenu
    global var
    global frame
    logMenu.grid_forget() #Just because there's a garbage collector doesn't mean I can't leak memory
    logMenu = Tk.OptionsMenu(frame, var, [f for f in os.listdir(dirStr) if '.txt' in f]).grid(row = 0, column = 0, columnspan=3)

#Parameter is the name of the log, that will become the open log
def set_active_log(log_name) :
    global band_beats
    global ear_beats
    global wrist_bpm
    (band_beats, ear_beats, wrist_bpm) = open_file(log_name)
    global plots
    for p in plots :
        p.killself()
    plots = []
    canvas.draw()

menubar = Menu(root)

filemenu = Menu(menubar, tearoff=0)
filemenu.add_command(label = 'Open', command=onOpenPicked)
filemenu.add_command(label = 'Open Folder', command=onOpenFolderPicked)
menubar.add_cascade(label = 'File', menu=filemenu)

root.config(menu=menubar)

frame = Frame(root)
frame.pack(side = TOP, expand=1, fill=Tk.BOTH)


logs = open_logs()
entryDict = {}


var = Tk.StringVar()
var.set(logs[0])
onLogPicked()
var.trace('w', lambda name, index, mode, var=var: onLogPicked())

logMenu = Tk.OptionMenu(frame, var, *logs).grid(row=0, column=0, columnspan=3)

#Create a drop down menu to choose between filtering types
filts = ['Filter Bpm', 'Smart Filter', 'Raw Bpm', 'Raw Beats']
filtVar = Tk.StringVar()
filtVar.set(filts[0])
filtVar.trace('w', lambda name, index, mode, var = filtVar : onFiltPicked())
filtMenu = Tk.OptionMenu(frame, filtVar, *filts).grid(row = 0, column = 3)

Button(frame, text='Ear Clip: (sample_interval, ave_interval, filter)', command = lambda : onButton('ear')).grid(row = 1, column=0, sticky = W)
entryDict['ear'] = Entry(frame)
entryDict['ear'].grid(row = 1, column = 1, sticky = W)

Button(frame, text='Chest Band: (sample_interval, ave_interval, filter)', command = lambda : onButton('band')).grid(row = 2, column=0, sticky = W)
entryDict['band'] = Entry(frame)
entryDict['band'].grid(row = 2, column = 1, sticky = W)

Button(frame, text='Wrist Strap: (sample_interval, ave_interval, filter)', command = lambda : onButton('wrist')).grid(row =3, column=0, sticky=W)
entryDict['wrist'] = Entry(frame)
entryDict['wrist'].grid(row = 3, column = 1, sticky = W)

Button(frame, text='All: (sample_interval, ave_interval, filter)', command = lambda : onButton('master')).grid(row = 4, column = 0, sticky = W)
mastVar = StringVar()
mastVar.trace('w', lambda name, index, mode, mastVar=mastVar : onMasterEntryChange(mastVar))
masterEntry = Entry(frame, textvariable=mastVar)
masterEntry.grid(row = 4, column = 1, sticky = W)
entryDict['master'] = masterEntry
mastVar.set('2, 1.5, 1') #set all boxes to default params

rowDict = {'ear' : 1, 'band' : 2, 'wrist' : 3}
colDict = {'ear' : 2, 'band' : 2, 'wrist' : 2}



'''
for log in logs :
    button = Button(frame, text = log, command = lambda x = log: onLogButtonClick(x)) #this line took me about an hour
    button.pack(side = TOP)
'''

###start copied code

wig.pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)

toolbar = NavigationToolbar2TkAgg(canvas, root)
toolbar.update()
canvas._tkcanvas.pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)



def on_key_event(event):
    print('you pressed %s' % event.key)
    key_press_handler(event, canvas, toolbar)

canvas.mpl_connect('key_press_event', on_key_event)


def _quit():
    root.quit()     # stops mainloop
    root.destroy()  # this is necessary on Windows to prevent
                    # Fatal Python Error: PyEval_RestoreThread: NULL tstate

button = Tk.Button(master=root, text='Quit', command=_quit)
button.pack(side=Tk.BOTTOM)

Tk.mainloop()
