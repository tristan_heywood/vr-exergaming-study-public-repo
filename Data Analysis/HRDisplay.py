import pyLgVw
from Tkinter import *
import tkFileDialog
import matplotlib
import BpmAlg
import Tkinter as Tk
import pdb
matplotlib.use('TkAgg')

from numpy import arange, sin, pi
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
# implement the default mpl key bindings
from matplotlib.backend_bases import key_press_handler


from matplotlib.figure import Figure

class Display :

    def __init__(self, person, retFigure = False) :
        self.person = person
        self.display(retFigure)

    @staticmethod
    def on_key_event(event):
        print('you pressed %s' % event.key)
        key_press_handler(event, canvas, toolbar)

    @staticmethod
    def _quit():
        root.quit()     # stops mainloop
        root.destroy()  # this is necessary on Windows to prevent
                        # Fatal Python Error: PyEval_RestoreThread: NULL tstate

    # def on_mov_ave_press(self) :
    #     print 'mov ave pressed'
    #     self.rawLine.set_visible(not self.rawLine.get_visible())
    #     self.canvas.draw()
    #
    # def on_ave_filt_press(self) :
    #     print 'filt ave pressed'
    #     self.filtLine.set_visible(not self.filtLine.get_visible())
    #     self.canvas.draw()
    #
    # def on_algorithm_press(self) :
    #     print 'on alg pressed'
    #     self.algLine.set_visible(not self.algLine.get_visible())
    #     self.canvas.draw()

    def flip_visible(self, line) :
        if isinstance(line, list) :
            for obj in line :
                obj.set_visible(not obj.get_visible())
        else :
            line.set_visible(not line.get_visible())
        self.canvas.draw()

    def display(self, retFigure = False) :
        root = Tk.Tk()
        root.wm_title("Embedding in TK")

        gridSpec = matplotlib.gridspec.GridSpec(2, 1, height_ratios=[7, 1])

        f = Figure(figsize=(12, 12), dpi=60)
        axes = f.add_subplot(gridSpec[0]) #211, 111
        axes2 = f.add_subplot(gridSpec[1], sharex=axes) #212

        self.rawBpmL = pyLgVw.beats_to_bpm(self.person.beats, 2, 1.5)
        self.rawX, self.rawY = [x[0] for x in self.rawBpmL], [x[1] for x in self.rawBpmL]
        self.rawLine = axes.plot(self.rawX, self.rawY)[0]

        self.filtBpmL = pyLgVw.filter(self.rawBpmL, 1.5)
        self.filtX, self.filtY = [x[0] for x in self.filtBpmL], [x[1] for x in self.filtBpmL]
        self.filtLine = axes.plot(self.filtX, self.filtY)[0]

        #if hasattr(self.person, 'smoothedX') :
        #    self.kLine = axes.plot(self.person.smoothedX, self.person.smoothedY)[0]

        algX, algY = [x[0] for x in self.person.bpmList], [x[1] for x in self.person.bpmList]
        self.algLine = axes.plot(algX, algY)[0]
        #axes.plot()

        self.annots = []
        for act in [act for act in self.person.activities if act.needRest or act.name == 'Portal'] :
            self.annots.append(axes.axvline(x = act.bpmL[0][0], linewidth = 1, color = 'k'))
            self.annots.append(axes.axvline(x = act.bpmL[-1][0], linewidth = 1, color = 'k'))
            self.annots.append(axes.annotate('Hot Squat' if act.name == 'Squat' else act.name, xy = (act.bpmL[0][0] + 5000, 50), size = 22))


        self.modLine = axes.axhline(y = self.person.max_hr() * 0.5, linewidth = 2, color = 'y')
        self.vigLine = axes.axhline(y = self.person.max_hr() * 0.7, linewidth = 2, color = 'm')

        smoothed = self.person.smooth_hr()
        smoothX = [x[0] for x in smoothed]
        smoothY = [x[1] for x in smoothed]
        self.smoothLine = axes.plot(smoothX, smoothY)[0]

        # a tk.DrawingArea
        self.canvas = FigureCanvasTkAgg(f, master=root)
        canvas = self.canvas
        if not retFigure :
            canvas.show()
        wig = canvas.get_tk_widget()

        frame = Frame(root)

        boxes = [('Moving Ave', self.rawLine), ('Moving Ave Filtered', self.filtLine), ('Algorithm', self.algLine), ('Show Games', self.annots), ('Smoothed HR', self.smoothLine), ('Intensity Lines', [self.modLine, self.vigLine])]
        for ind, (name, line) in enumerate(boxes) :
            but = Tk.Checkbutton(frame, text=name, command = lambda line = line : self.flip_visible(line)) #the line=line stuff is really stretching my understanding tbh
            but.grid(row = ind, column = 0, sticky = W)
            but.select()

        wsBeatsL, divL = BpmAlg.calc_bpm(self.person.beats, True)
        axes2.scatter(self.person.beats, [0] * len(self.person.beats), marker = '.')
        axes2.scatter(wsBeatsL, [300] * len(wsBeatsL), marker = '.')
        axes2.scatter(divL, [250] * len(divL))
        axes2.scatter([x[0] for x in self.person.bpmList], [200] * len(self.person.bpmList), marker = '.')

        # Tk.Checkbutton(frame, text='Moving Ave', command = lambda : self.flip_visible(self.rawLine)).grid(row = 0, column = 0, sticky = W)
        # Tk.Checkbutton(frame, text='Moving Ave Filtered', command = lambda : self.flip_visible(self.filtLine)).grid(row = 1, column = 0, sticky = W)
        # Tk.Checkbutton(frame, text='Algorithm', command = lambda : self.flip_visible(self.algLine)).grid(row = 2, column = 0, sticky = W)

        frame.pack(side = TOP, expand=1, fill=Tk.BOTH)

        wig.pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)

        toolbar = NavigationToolbar2TkAgg(canvas, root)
        toolbar.update()
        canvas._tkcanvas.pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)

        canvas.mpl_connect('key_press_event', self.on_key_event)

        button = Tk.Button(master=root, text='Quit', command=self._quit)
        button.pack(side=Tk.BOTTOM)

        if retFigure :
            f.savefig(self.person.name + '_graph.png', bbox_inches='tight')
        else :
            Tk.mainloop()
