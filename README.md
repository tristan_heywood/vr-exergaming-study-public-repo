# Code for VR Exergaming Study #

This code was written for a 12 week summer project running from 2016 to 2017. The study examined the use of virtual reality of exercising. The motivation behind the study was that VR games have the potential to make exercising fun, therefore helping people movitate themselves to exercise. We found that participants experienced substantial increases in heart rate while playing a selection of VR games, indiciated that the games were indeed providing exercise. Two papers produced from this study can be found here: https://scholar.google.com.au/citations?user=Rca1XbsAAAAJ&hl=en

There are two parts to this repository. 

## HR Data Collection ##

This folder contains the programs that were responsible for measuring the participant's heart rate and transmitting it back to a nearby desktop for processing. The primary method of measuring the participant's heart rates was through a Polar E30 chest strap heart rate monitor. Unfortunately, this strap used the ANT protocol for transmitting the collected data. This protocol by design had an effective range of about 1 meter. As a result the data had to be collected by an arduino connected to an ANT reciever held on the participant'd body. In particular, a Moteino supporting LoRa (a low bit rate rf network) was used. This device transmitted the collected data to a reciever LoRa compatible Moteino which was connected to a desktop computer. At one point in the study, different methods of heart rate collection were being evaluated (ear clip, wrist strap and chest strap). Due to a shortage of interrupt enabled pins, polling had to be used for some data sources. 

Once the data was recieved by the reciever Moteino, it was sent by serial to the desktop where it was read by a python script. This script stored the data to a log file, while also doing some rudimentary signal processing to display the signal in real time. The script sends the data via an http get request to a node-RED server, which displayed the heart rate using a gauge animation. 

## HR Data Analysis

Within the Data Analysis folder there is code to read the logs and store the information in a series of objects to make it easier to deal with. The logs are read in and correlated with a list of what games were being played at what times so that each participant object had a list of game objects, each of which contained the heart rate data relevant to that game. The various objects were stored by serialisation using cpickle and unpickled automatically as required by the program. 


The data can be output to an excel spreadsheet for ease of manipulation by other group members. BpmAlg.py contains the algorithm used to clean up the bpm signal. HRDispay and KinGraph show graphs of the heart rate and kinect joint tracking data respectively. HRGui was used to evaulate different ways of processing the HR data. 