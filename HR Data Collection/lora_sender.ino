// rf95_client.pde
// -*- mode: C++ -*-
// Example sketch showing how to create a simple messageing client
// with the RH_RF95 class. RH_RF95 class does not provide for addressing or
// reliability, so you should only use RH_RF95 if you do not need the higher
// level messaging abilities.
// It is designed to work with the other example rf95_server
// Tested with Moteino-LoRa, MoteinoMEGA-LoRa

#include <SPI.h>
#include <RH_RF95.h>

#define PIN_EAR 3
#define PIN_BAND 4
#define FREQUENCY  915


#define LED           9 // Moteinos have LEDs on D9
#define FLASH_SS      8 // and FLASH SS on D8


unsigned volatile long lastBeatTimeEar = 0; 
volatile bool lastBeatSeenEar = true; 

byte oldSample, sample; 


// Singleton instance of the radio driver
RH_RF95 rf95;

void setup() 
{
  Serial.begin(9600);
  if (!rf95.init())
    Serial.println("init failed");
  else { Serial.print("init OK - "); Serial.print(FREQUENCY); Serial.print("mhz"); }

  attachInterrupt(digitalPinToInterrupt(PIN_EAR), interrupt_ear, RISING); 
  
  // Defaults after init are 434.0MHz, 13dBm, Bw = 125 kHz, Cr = 4/5, Sf = 128chips/symbol, CRC on
  rf95.setFrequency(FREQUENCY);
  //rf95.setFrequency(915);
  Serial.println("Setup compelete"); 
}

void loop()
{

  String str = ""; 
  if(!lastBeatSeenEar) {
    str = str + "Ear Beat: " + lastBeatTimeEar;   
  }
  
  sample = digitalRead(PIN_BAND); 
  if(sample && (oldSample != sample)) {
    if(str.length() > 0) str = str + "\n"; 
    str = str + "Band Beat: " + millis(); 
  }
  if(str.length() > 0) {
    uint8_t data[50];
    str.getBytes(data, 50); 
    lastBeatSeenEar = true; 
    
    Serial.print("Sending to rf95_server: ");
    Serial.println(str); 
    rf95.send(data, sizeof(data)); 
    rf95.waitPacketSent(); 
    Blink(LED,3);
  }
}

void Blink(byte PIN, int DELAY_MS)
{
  pinMode(PIN, OUTPUT);
  digitalWrite(PIN,HIGH);
  delay(DELAY_MS);
  digitalWrite(PIN,LOW);
}
void interrupt_ear() {
  lastBeatTimeEar = millis(); 
  lastBeatSeenEar = false; 
}
