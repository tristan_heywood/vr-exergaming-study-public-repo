from __future__ import division 
import serial
import time
import requests
import re

def write_http(bpm) :
	try : 
		print 'sending: ' + str(bpm)
		geturl = '[REMOVED]' + str(bpm)
		requests.get(geturl) 
	except IndexError : 
		print 'Write to HTTP error' 

		
#counts the number of beats in the last 'seconds' seconds 
def calc_HR(beats, ind, seconds) :
	seconds *= 1000
	numBeats = 0
	start = beats[ind] 
	end = 0
	while start - beats[ind] < seconds and ind >= 0: 
		numBeats += 1
		end = beats[ind]
		ind -= 1
	if start == end : return 0 #if there were no beats in the interval, return zero. This kills the graph
	millis_per_beat = (start - end) // (numBeats - 1)
	return 60000 // millis_per_beat
	
ser = serial.Serial(
	port = 'COM4',
	baudrate=9600, 
	parity = serial.PARITY_ODD,
	stopbits = serial.STOPBITS_TWO,
	bytesize = serial.SEVENBITS)
	
ser.isOpen() 

sample_rate = 0.3 #seconds between bpm calculations 
ave_interval = 5 #seconds of beats used in the moving average
max_change = 4 #maximum change in bpm per second - used for filtering. Set to something huge for no filtering (1000)
max_wait = 5 #maximum wait between bpm writes. Forces a reading through the filter even if the rate of change is too high

last_samp = 0 #time stamp of last sample 

timestr = time.strftime('%Y-%m-%d, %H.%M.%S HR Log.txt')
logFile = open(timestr, 'w')

bbeats = [] 
ebeats = [] 
bpmlist = [] #list of tuples of the form (timestamp, bpm)

while 1 : 
	out = '' 
	buff = ''
	
	while ser.inWaiting() > 0 : 
		out = ser.readline()
		line = out.strip()
		
		match = re.search('(\w+) Beat: (\d+)', line)
		if not match is None :
			time_stp = int(match.group(2).strip())
			if match.group(1) == 'Band' : bbeats.append(time_stp)
			elif match.group(1) == 'Ear' : ebeats.append(time_stp) 
			
		if len(ebeats) > 0 and ebeats[-1] - last_samp > (sample_rate * 1000) : 
			bpm = calc_HR(ebeats, len(ebeats) - 1, ave_interval) 
			if len(bpmlist) == 0 : bpmlist.append((ebeats[-1], bpm))
			else : 
				bpm_dif = bpm - bpmlist[-1][1]
				time_dif = ebeats[-1] - bpmlist[-1][0] 
				if abs(bpm_dif / (time_dif / 1000)) <= max_change or time_dif > max_wait * 1000: 
					bpmlist.append((ebeats[-1], bpm))
					write_http(bpmlist[-1][1])
			last_samp = ebeats[-1] 
		
		logLine = time.strftime('%H:%M:%S ') + line;  
		logFile.write(logLine + '\n')
		print "logging: " + logLine